# Breadcrumb Menu

To add an additional item to a breadcrumb menu on glossary detail pages, you can use the `AddPortfolioToMenuProcessor`.
To do this, add the following TypoScript to the setup section of your site package extension. We assume here that your main `FLUIDTEMPLATE` is in `page.10`.

```typo3_typoscript
page.10 = FLUIDTEMPLATE
page.10 {
    # [...] template settings
    dataProcessing {
        # [...] Other data processors
        50 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
        50 {
            as = breadcrumbMenu
            special = rootline
        }
        60 = CodingMs\Portfolios\DataProcessing\AddPortfolioToMenuProcessor
        60.menus = breadcrumbMenu
    }
}
```

The property `menus` of the `AddPortfolioToMenuProcessor` must contain the key of the `MenuProcessor` containing your breadcrumb. You can use more than one menu here by specifying multiple keys as a comma-separated list. For example: `60.menus = breadcrumbMenu,myOtherBreadcrumb`.
