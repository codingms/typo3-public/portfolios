# Sprechende URLs

## Slug-Konfiguration (ab TYPO3 9.5)

```yaml
routeEnhancers:
  PageTypeSuffix:
    type: PageType
    map:
      portfolio.pdf: 1491899512
      portfolio.json: 1491378305
  PortfoliosPlugin:
    type: Extbase
    limitToPages:
      - 21
      - 22
    extension: Portfolios
    plugin: List
    routes:
      -
        routePath: '/{portfolios_slug}'
        _controller: 'Portfolio::show'
        _arguments:
          portfolios_slug: portfolio
    defaultController: 'Portfolio::list'
    aspects:
      portfolios_slug:
        type: PersistedAliasMapper
        tableName: tx_portfolios_domain_model_portfolio
        routeFieldName: slug
        routeValuePrefix: /
```


## Realurl-Konfiguration (bis TYPO3 9.5)

```php
$configuration = [

    'fixedPostVars' => [
        'portfoliosConfiguration' => [
            0 => [
                'GETvar' => 'tx_portfolios_list[action]',
                'valueMap' => [
                    'show' => '',
                ],
                'noMatch' => 'bypass',
            ],
            1 => [
                'GETvar' => 'tx_portfolios_list[controller]',
                'valueMap' => [],
                'noMatch' => 'bypass',
            ],
            2 => [
                'GETvar' => 'tx_portfolios_list[portfolio]',
                'lookUpTable' => [
                    'table' => 'tx_portfolios_domain_model_portfolio',
                    'id_field' => 'uid',
                    'alias_field' => 'title',
                    //'alias_field' => "CONCAT(title, '-', sub_title)",
                    'addWhereClause' => ' AND NOT deleted',
                    'useUniqueCache' => 1,
                    'useUniqueCache_conf' => [
                        'strtolower' => 1,
                        'spaceCharacter' => '-',
                    ],
                    'languageGetVar' => 'L',
                    'languageExceptionUids' => '',
                    'languageField' => 'sys_language_uid',
                    'transOrigPointerField' => 'l10n_parent',
                    'autoUpdate' => 1,
                    'expireDays' => 180,
                ],
            ],
        ],
        62 => 'portfoliosConfiguration',
    ],

];
```
