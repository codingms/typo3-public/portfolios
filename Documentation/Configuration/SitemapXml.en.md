# Sitemap.xml configuration for products

The following configuration enables you to generate a Sitemap.xml for your portfolios:

```typo3_typoscript
plugin.tx_seo {
    config {
        xmlSitemap {
            sitemaps {
                portfolios {
                    provider = TYPO3\CMS\Seo\XmlSitemap\RecordsXmlSitemapDataProvider
                    config {
                        table = tx_portfolios_domain_model_portfolio
                        sortField = sorting
                        lastModifiedField = tstamp
                        recursive = 1
                        pid = 1976
                        url {
                            pageId = 1980
                            fieldToParameterMap {
                                uid = tx_portfolios_list[portfolio]
                            }
                            additionalGetParameters {
                                tx_portfolios_list.controller = Portfolio
                                tx_portfolios_list.action = show
                            }

                            useCacheHash = 1
                        }
                    }
                }
            }
        }
    }
}
```
