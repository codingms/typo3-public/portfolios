# Portfolios Migration

## Version 3.0.0

* If you're using own Fluid-Templates, you might need to remove the parameter `noCacheHash="1"`.


## Migration from ftm_ext_references

Change database tables and references:

```sql
RENAME TABLE `tx_ftmextreferences_domain_model_reference` TO `tx_portfolios_domain_model_portfolio`;
RENAME TABLE `tx_ftmextreferences_domain_model_referencecategory` TO `tx_portfolios_domain_model_portfoliocategory`;
RENAME TABLE `tx_ftmextreferences_domain_model_referencetag` TO `tx_portfolios_domain_model_portfoliotag`
RENAME TABLE `tx_ftmextreferences_domain_model_referencetagcategory` TO `tx_portfolios_domain_model_portfoliotagcategory`
RENAME TABLE `tx_ftmextreferences_reference_referencetag_mm` TO `tx_portfolios_portfolio_portfoliotag_mm`;
RENAME TABLE `tx_ftmextreferences_reference_referencecategory_mm` TO `tx_portfolios_portfolio_portfoliocategory_mm`;
UPDATE `sys_file_reference` SET `tablenames` = 'tx_portfolios_domain_model_portfolio' WHERE tablenames = 'tx_ftmextreferences_domain_model_reference';
UPDATE `tt_content` SET `list_type` = 'portfolios_list' WHERE `list_type` = 'ftmextreferences_references';

# transfer teaser text to new field
UPDATE `tx_portfolios_domain_model_portfolio` SET `teaser` = `teaser_text`;
```

* If you've overridden some Partials/Templates you need to rename the folder `Reference` into `Portfolio`
* Replace `partial="Reference/` in files with `partial="Portfolio/`
* Replace `{portfolio.teaserText}` in files with `{portfolio.teaser}`
* Re-add the static TypoScript template
