<?php

namespace CodingMs\Portfolios\Domain\Model;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Domain\Model\Traits\DescriptionTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\HtmlTitleTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\MarkdownTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\MetaAbstractTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\MetaDescriptionTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\MetaKeywordsTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\TeaserTrait;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Annotation as Extbase;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute1Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute2Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute3Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute4Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute5Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute6Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute7Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute8Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\CreationDateTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\CreationUserTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\HiddenTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\ModificationDateTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\TitleTrait;
use CodingMs\Portfolios\Domain\Repository\FileReferenceRepository;

/**
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class Portfolio extends AbstractEntity
{
    use HiddenTrait;
    use CreationDateTrait;
    use ModificationDateTrait;
    use CreationUserTrait;
    use TitleTrait;
    use Attribute1Trait;
    use Attribute2Trait;
    use Attribute3Trait;
    use Attribute4Trait;
    use Attribute5Trait;
    use Attribute6Trait;
    use Attribute7Trait;
    use Attribute8Trait;
    use MetaAbstractTrait;
    use MetaKeywordsTrait;
    use MetaDescriptionTrait;
    use HtmlTitleTrait;
    use MarkdownTrait;
    use TeaserTrait;
    use DescriptionTrait;

    /**
     * @var string
     */
    protected string $subTitle;

    /**
     * @var bool
     */
    protected bool $highlight;

    /**
     * @var ObjectStorage<FileReference>
     * @Extbase\ORM\Lazy
     */
    protected ObjectStorage $images;

    /**
     * @var ObjectStorage<FileReference>
     * @Extbase\ORM\Lazy
     */
    protected ObjectStorage $files;

    /**
     * @var int
     */
    protected int $workingHours;

    /**
     * @var \DateTime
     */
    protected \DateTime $finishingDate;

    /**
     * @var string
     */
    protected string $www;

    /**
     * @var ObjectStorage<PortfolioCategory>
     * @Extbase\ORM\Lazy
     */
    protected ObjectStorage $categories;

    /**
     * @var ObjectStorage<PortfolioTag>
     * @Extbase\ORM\Lazy
     */
    protected ObjectStorage $tags;

    /**
     * @var ObjectStorage<Portfolio>
     * @Extbase\ORM\Lazy
     */
    protected ObjectStorage $relatedPortfolios;

    /**
     * @var float
     */
    protected float $latitude;

    /**
     * @var float
     */
    protected float $longitude;

    public function __construct()
    {
        $this->categories = new ObjectStorage();
        $this->tags = new ObjectStorage();
        $this->images = new ObjectStorage();
        $this->files = new ObjectStorage();
    }

    /**
     * Returns the subTitle
     *
     * @return string $subTitle
     */
    public function getSubTitle(): string
    {
        return $this->subTitle;
    }

    /**
     * @param string $subTitle
     * @return void
     */
    public function setSubTitle(string $subTitle): void
    {
        $this->subTitle = $subTitle;
    }

    /**
     * @return bool
     */
    public function getHighlight(): bool
    {
        return $this->highlight;
    }

    /**
     * @param bool $highlight
     * @return void
     */
    public function setHighlight(bool $highlight): void
    {
        $this->highlight = $highlight;
    }

       /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescriptionForPdf(): string
    {
        $description = str_replace('<br', "\n<br", $this->description);
        $description = str_replace('</p', "\n</p", $description);
        $description = str_replace('<br /> ', '<br />', $description);
        return strip_tags($description);
    }

    /**
     * Adds a image
     *
     * @param FileReference $image
     * @return void
     */
    public function addImage(FileReference $image): void
    {
        $this->images->attach($image);
    }

    /**
     * Removes a image
     *
     * @param FileReference $imageToRemove The image to be removed
     * @return void
     */
    public function removeImage(FileReference $imageToRemove): void
    {
        $this->images->detach($imageToRemove);
    }

    /**
     * Returns the images
     *
     * @return ObjectStorage<FileReference> $images
     */
    public function getImages(): ObjectStorage
    {
        return $this->images;
    }

    /**
     * Sets the images
     *
     * @param ObjectStorage<FileReference> $images
     * @return void
     */
    public function setImages(ObjectStorage $images): void
    {
        $this->images = $images;
    }

    /**
     * Adds a image
     *
     * @param FileReference $file
     * @return void
     */
    public function addFile(FileReference $file): void
    {
        $this->files->attach($file);
    }

    /**
     * Removes a file
     *
     * @param FileReference $fileToRemove The file to be removed
     * @return void
     */
    public function removeFile(FileReference $fileToRemove): void
    {
        $this->files->detach($fileToRemove);
    }

    /**
     * Returns the files
     *
     * @return array<int, mixed>|ObjectStorage
     */
    public function getFiles()
    {
        $files = $this->files;
        if (count($this->files) > 0) {
            if (!$this->getIsNotTranslated()) {
                $files = [];
                /** @var FileRepository $fileRepository */
                $fileRepository = GeneralUtility::makeInstance(FileRepository::class);
                $fileObjects = $fileRepository->findByRelation(
                    'tx_portfolios_domain_model_portfolio',
                    'files',
                    (int)$this->_localizedUid
                );
                if (count($fileObjects) > 0) {
                    /** @var FileReference $fileObject */
                    foreach ($fileObjects as $fileObject) {
                        $fileReferenceUid = (int)$fileObject->getUid();
                        $fileReferenceRepository = GeneralUtility::makeInstance(FileReferenceRepository::class);
                        $files[] = $fileReferenceRepository->findByIdentifier($fileReferenceUid);
                    }
                }
            }
        }
        return $files;
    }

    /**
     * Identify, if this is a non translated record.
     * This means this record is in default language.
     * @return bool
     */
    public function getIsNotTranslated(): bool
    {
        return $this->_localizedUid === $this->uid;
    }

    /**
     * Sets the files
     *
     * @param ObjectStorage<FileReference> $files
     * @return void
     */
    public function setFiles(ObjectStorage $files): void
    {
        $this->files = $files;
    }

    /**
     * Returns the finishingDate
     *
     * @return \DateTime $finishingDate
     */
    public function getFinishingDate(): \DateTime
    {
        return $this->finishingDate;
    }

    /**
     * Gets the workingHours
     *
     * @return int
     */
    public function getWorkingHours(): int
    {
        return $this->workingHours;
    }

    /**
     * Sets the workingHours
     *
     * @param int $workingHours
     * @return void
     */
    public function setWorkingHours(int $workingHours): void
    {
        $this->workingHours = $workingHours;
    }

    /**
     * Sets the finishingDate
     *
     * @param \DateTime $finishingDate
     * @return void
     */
    public function setFinishingDate(\DateTime $finishingDate): void
    {
        $this->finishingDate = $finishingDate;
    }

    /**
     * Gets the finishingDate as string
     *
     * @return string
     */
    public function getFinishingDateString(): string
    {
        $date = '';
        $timestamp = $this->finishingDate->getTimestamp();
        if ($timestamp > 0) {
            $date = date('d.m.Y', $timestamp);
        }
        return $date;
    }

    /**
     * Returns the www
     *
     * @return string $www
     */
    public function getWww(): string
    {
        return $this->www;
    }

    /**
     * Sets the www
     *
     * @param string $www
     * @return void
     */
    public function setWww($www): void
    {
        $this->www = $www;
    }

    /**
     * Adds a PortfolioCategory
     *
     * @param PortfolioCategory $category
     * @return void
     */
    public function addCategory(PortfolioCategory $category): void
    {
        $this->categories->attach($category);
    }

    /**
     * Removes a PortfolioCategory
     *
     * @param PortfolioCategory $categoryToRemove The PortfolioCategory to be removed
     * @return void
     */
    public function removeCategory(PortfolioCategory $categoryToRemove): void
    {
        $this->categories->detach($categoryToRemove);
    }

    /**
     * Returns the categories
     *
     * @return ObjectStorage<PortfolioCategory> $categories
     */
    public function getCategories(): ObjectStorage
    {
        return $this->categories;
    }

    /**
     * Sets the categories
     *
     * @param ObjectStorage<PortfolioCategory> $categories
     * @return void
     */
    public function setCategories(ObjectStorage $categories): void
    {
        $this->categories = $categories;
    }

    /**
     * Returns the categories as a String
     *
     * @return string
     */
    public function getCategoriesString(): string
    {
        $categoriesArray = [];
        $categories = $this->getCategories();
        if ($categories->count() > 0) {
            foreach ($categories as $category) {
                $categoriesArray[] = $category->getTitle();
            }
        }
        return implode(', ', $categoriesArray);
    }

    /**
     * Adds a PortfolioTag
     *
     * @param PortfolioTag $tag
     * @return void
     */
    public function addTag(PortfolioTag $tag): void
    {
        $this->tags->attach($tag);
    }

    /**
     * Removes a PortfolioTag
     *
     * @param PortfolioTag $tagToRemove The PortfolioTag to be removed
     * @return void
     */
    public function removeTag(PortfolioTag $tagToRemove): void
    {
        $this->tags->detach($tagToRemove);
    }

    /**
     * Returns the tags
     *
     * @return ObjectStorage<PortfolioTag> $tags
     */
    public function getTags(): ObjectStorage
    {
        return $this->tags;
    }

    /**
     * Sets the tags
     *
     * @param ObjectStorage<PortfolioTag> $tags
     * @return void
     */
    public function setTags(ObjectStorage $tags): void
    {
        $this->tags = $tags;
    }

    /**
     * Returns the tags as a String
     *
     * @return string
     */
    public function getTagsString(): string
    {
        $tagsArray = [];
        $tags = $this->getTags();
        if ($tags->count() >0) {
            foreach ($tags as $tag) {
                $tagsArray[] = $tag->getTitle();
            }
        }
        return implode(', ', $tagsArray);
    }

    /**
     * Adds a Portfolio
     *
     * @param Portfolio $relatedPortfolio
     * @return void
     */
    public function addRelatedPortfolio(Portfolio $relatedPortfolio): void
    {
        $this->relatedPortfolios->attach($relatedPortfolio);
    }

    /**
     * Removes a Portfolio
     *
     * @param Portfolio $relatedPortfolioToRemove The Portfolio to be removed
     * @return void
     */
    public function removeRelatedPortfolio(Portfolio $relatedPortfolioToRemove): void
    {
        $this->relatedPortfolios->detach($relatedPortfolioToRemove);
    }

    /**
     * Returns the relatedPortfolios
     *
     * @return ObjectStorage<Portfolio> $relatedPortfolios
     */
    public function getRelatedPortfolios(): ObjectStorage
    {
        return $this->relatedPortfolios;
    }

    /**
     * Sets the relatedPortfolios
     *
     * @param ObjectStorage<Portfolio> $relatedPortfolios
     * @return void
     */
    public function setRelatedPortfolios(ObjectStorage $relatedPortfolios): void
    {
        $this->relatedPortfolios = $relatedPortfolios;
    }

    /**
     * Creates required CSS classes
     * @return string
     */
    public function getCssClasses(): string
    {
        $classes = [];
        /** @var PortfolioTag $tag */
        foreach ($this->tags as $tag) {
            $classes[] = 'portfolio-tag-' . $tag->getUid();
        }
        /** @var PortfolioCategory $category */
        foreach ($this->categories as $category) {
            $classes[] = 'portfolio-category-' . $category->getUid();
        }
        if ($this->getHighlight()) {
            $classes[] = 'highlight';
        }
        return implode(' ', $classes);
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     * @return void
     */
    public function setLatitude(float $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     * @return void
     */
    public function setLongitude(float $longitude): void
    {
        $this->longitude = $longitude;
    }
}
