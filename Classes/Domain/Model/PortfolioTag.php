<?php

namespace CodingMs\Portfolios\Domain\Model;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Domain\Model\Traits\DescriptionTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\ImageTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\SortingTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\TitleTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\VariantStringTrait;
use TYPO3\CMS\Extbase\Annotation as Extbase;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class PortfolioTag extends AbstractEntity
{
    use TitleTrait;
    use ImageTrait;
    use SortingTrait;
    use DescriptionTrait;
    use VariantStringTrait;

    /**
     * @var ?PortfolioTagCategory
     * Don't use lazy annotation, because of a mysterious loading issue!!
     */
    protected ?PortfolioTagCategory $category;

    /**
     * @return ?PortfolioTagCategory $category
     */
    public function getCategory(): ?PortfolioTagCategory
    {
        return $this->category;
    }

    /**
     * @param PortfolioTagCategory $category
     * @return void
     */
    public function setCategory(PortfolioTagCategory $category): void
    {
        $this->category = $category;
    }

}
