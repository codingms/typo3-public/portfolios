<?php

namespace CodingMs\Portfolios\Domain\Repository;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

/**
 * Portfolio category repository
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class PortfolioCategoryRepository extends DefaultRepository
{

    /**
     * Find all ordered by ...
     * @param array<int, int|string> $allowed Array with allowed categories
     * @param string $sorting Sort by field
     * @return array<mixed>|QueryResultInterface
     * @throws InvalidQueryException
     */
    public function findAllOrderedBy(array $allowed = [], string $sorting = 'title')
    {
        $query = $this->createQuery();
        if (is_array($allowed) && count($allowed) > 0) {
            $query->matching(
                $query->in('uid', $allowed)
            );
        }
        if (trim($sorting) !== '') {
            $query->setOrderings([$sorting => QueryInterface::ORDER_ASCENDING]);
        }
        return $query->execute();
    }

    /**
     * Get first array item
     * @param array<mixed> $array
     * @return mixed
     */
    protected function gfi(array $array)
    {
        $array1 = array_values($array);
        return array_shift($array1);
    }

    /**
     * @param array<string, mixed> $filter
     * @param bool $count
     * @return array<mixed>|QueryResultInterface|int
     * @throws InvalidQueryException
     */
    public function findAllForBackendList(array $filter = [], bool $count = false)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(false);
        $constraints = [];
        if (!empty($filter['searchWord'])) {
            $constraintsSearchWord = [];
            $constraintsSearchWord[] = $query->like('title', '%' . $filter['searchWord'] . '%');
            $constraintsSearchWord[] = $query->like('description', '%' . $filter['searchWord'] . '%');
            $constraints[] = $query->logicalOr(...$constraintsSearchWord);
        }
        $constraints[] = $query->equals('pid', $filter['pid']);
        if (count($constraints) > 1) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        } else {
            $query->matching($this->gfi($constraints));
        }
        if (!$count) {
            if (isset($filter['sortingField']) && $filter['sortingField'] != '') {
                if ($filter['sortingOrder'] == 'asc') {
                    $query->setOrderings([(string)$filter['sortingField'] => QueryInterface::ORDER_ASCENDING]);
                } else {
                    if ($filter['sortingOrder'] == 'desc') {
                        $query->setOrderings([(string)$filter['sortingField'] => QueryInterface::ORDER_DESCENDING]);
                    }
                }
            }
            if ((int)$filter['limit'] > 0) {
                $query->setOffset((int)$filter['offset']);
                $query->setLimit((int)$filter['limit']);
            }
            return $query->execute();
        }
        return $query->execute()->count();
    }

}
