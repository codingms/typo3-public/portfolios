<?php

namespace CodingMs\Portfolios\Domain\Repository;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Portfolios\Domain\Model\PortfolioCategory;
use CodingMs\Portfolios\Domain\Model\PortfolioTag;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

/**
 * Portfolios repository
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class PortfolioRepository extends DefaultRepository
{
    protected $defaultOrderings = [
        'title' => QueryInterface::ORDER_ASCENDING
    ];

    /**
     * @param array<string, mixed> $filter
     * @return array<mixed>|QueryResultInterface
     * @throws InvalidQueryException
     */
    public function findAllByFilter(array $filter = [])
    {
        $query = $this->createQuery();
        $constraints = [];
        //
        // processes the search word
        if (isset($filter['word']['value']) && trim($filter['word']['value']) != '') {
            $constraintsOr = [];
            if (count($filter['word']['queryFields']) > 0) {
                foreach ($filter['word']['queryFields'] as $queryField) {
                    $constraintsOr[] = $query->like($queryField, '%' . $filter['word']['value'] . '%');
                }
            } else {
                $constraintsOr[] = $query->like('title', '%' . $filter['word']['value'] . '%');
                $constraintsOr[] = $query->like('subTitle', '%' . $filter['word']['value'] . '%');
                $constraintsOr[] = $query->like('teaser', '%' . $filter['word']['value'] . '%');
                $constraintsOr[] = $query->like('description', '%' . $filter['word']['value'] . '%');
                $constraintsOr[] = $query->like('markdown', '%' . $filter['word']['value'] . '%');
            }
            $constraints[] = $query->logicalOr(...$constraintsOr);
        }
        //
        // Filter by categories
        if (!empty($filter['category']) && !empty($filter['category']['values'])) {
            if (is_array($filter['category']['values']) && count($filter['category']['values']) > 0) {
                $constraintsCategory = [];
                /** @var PortfolioCategory $category */
                foreach ($filter['category']['values'] as $category) {
                    $constraintsCategory[] = $query->contains('categories', $category);
                }
                if (count($constraintsCategory) == 1) {
                    $constraints[] = $this->gfi($constraintsCategory);
                } else {
                    /**
                     * Changed to logicalOr by building version 1.2.0
                     */
                    $constraints[] = $query->logicalOr(...$constraintsCategory);
                }
            }
        }
        //
        // Filter by tags
        if (!empty($filter['tag']) && !empty($filter['tag']['values'])) {
            if (is_array($filter['tag']['values']) && count($filter['tag']['values']) > 0) {
                $constraintsTag = [];
                //
                switch ((int)$filter['queryType']) {
                    case 0:
                        // Simple OR
                        /** @var PortfolioTag $tag */
                        foreach ($filter['tag']['values'] as $tag) {
                            $constraintsTag[] = $query->contains('tags', $tag);
                        }
                        if (count($constraintsTag) == 1) {
                            $constraints[] = $this->gfi($constraintsTag);
                        } else {
                            $constraints[] = $query->logicalOr(...$constraintsTag);
                        }
                        break;
                    case 1:
                        // Simple AND
                        /** @var PortfolioTag $tag */
                        foreach ($filter['tag']['values'] as $tag) {
                            $constraintsTag[] = $query->contains('tags', $tag);
                        }
                        if (count($constraintsTag) === 1) {
                            $constraints[] = $this->gfi($constraintsTag);
                        } else {
                            $constraints[] = $query->logicalAnd(...$constraintsTag);
                        }
                        break;
                    case 2:
                        // Tags in TagCategory OR, and that result by AND
                        foreach ($filter['tag']['values'] as $tag) {
                            // First of all, group by TagCategory
                            $constraintsTag[$tag][] = $query->contains('tags', $tag);
                        }
                        foreach ($constraintsTag as $constraintsTagCategoryUid => $constraintsTagCategoryTags) {
                            if (count($constraintsTagCategoryTags) === 1) {
                                $constraintsTag[$constraintsTagCategoryUid] = $this->gfi($constraintsTagCategoryTags);
                            } else {
                                $constraintsTag[$constraintsTagCategoryUid] = $query->logicalOr(...$constraintsTagCategoryTags);
                            }
                        }
                        if (count($constraintsTag) === 1) {
                            $constraints[] = $this->gfi($constraintsTag);
                        } else {
                            $constraints[] = $query->logicalAnd(...$constraintsTag);
                        }
                        break;
                }
            }
        }
        //
        // Portfolios must be in pre filtered array
        if (isset($filter['preFiltered']) && count($filter['preFiltered']) > 0) {
            $constraints[] = $query->in('uid', $filter['preFiltered']);
        }
        //
        //
        if (!empty($constraints)) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        }
        if (isset($filter['offset'])) {
            $offset = (int)$filter['offset'];
            if ($offset > 0) {
                $query->setOffset((int)$filter['offset']);
            }
        }
        if (isset($filter['limit'])) {
            $limit = (int)$filter['limit'];
            if ($limit > 0) {
                $query->setLimit((int)$filter['limit']);
            }
        }
        //
        // Sorting
        if (isset($filter['sort']['by'])) {
            if ($filter['sort']['order'] === 'desc') {
                $query->setOrderings([(string)$filter['sort']['by'] => QueryInterface::ORDER_DESCENDING]);
            } else {
                $query->setOrderings([(string)$filter['sort']['by'] => QueryInterface::ORDER_ASCENDING]);
            }
        }
        return $query->execute();
    }

    /**
     * Get first array item
     * @param array<mixed> $array
     * @return mixed
     */
    protected function gfi(array $array)
    {
        $array1 = array_values($array);
        return array_shift($array1);
    }

    /**
     * Find all portfolios containing param tag
     *
     * @param PortfolioTag $portfolioTag
     * @return array<mixed>|QueryResultInterface
     * @throws InvalidQueryException
     */
    public function findAllByTags(PortfolioTag $portfolioTag)
    {
        $query = $this->createQuery();
        $constraints = [];
        // processes the category
        $constraints[] = $query->contains('tags', $portfolioTag);
        $query->matching(
            $query->logicalAnd(...$constraints)
        );
        return $query->execute();
    }

    /**
     * Find data by uid
     *
     * @param ?PortfolioCategory $category
     * @return array<mixed>|QueryResultInterface
     * @throws InvalidQueryException
     */
    public function findAllByCategory(PortfolioCategory $category = null)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        // processes the category
        if ($category instanceof PortfolioCategory) {
            $query->matching(
                $query->contains('categories', $category)
            );
        }
        return $query->execute();
    }

    /**
     * @param array<string, mixed> $filter
     * @param bool $count
     * @return array<mixed>|QueryResultInterface|int
     * @throws InvalidQueryException
     */
    public function findAllForBackendList(array $filter = [], bool $count = false)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(false);
        $constraints = [];
        if ($filter['category']['selected'] > 0) {
            $constraints[] = $query->contains('categories', $filter['category']['selected']);
        }
        if ($filter['tag']['selected'] > 0) {
            $constraints[] = $query->contains('tags', $filter['tag']['selected']);
        }
        $constraints[] = $query->equals('pid', $filter['pid']);
        if (count($constraints) > 1) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        } else {
            $query->matching($this->gfi($constraints));
        }
        if (!$count) {
            if (isset($filter['sortingField']) && $filter['sortingField'] != '') {
                if ($filter['sortingOrder'] == 'asc') {
                    $query->setOrderings([(string)$filter['sortingField'] => QueryInterface::ORDER_ASCENDING]);
                } else {
                    if ($filter['sortingOrder'] == 'desc') {
                        $query->setOrderings([(string)$filter['sortingField'] => QueryInterface::ORDER_DESCENDING]);
                    }
                }
            }
            if ((int)$filter['limit'] > 0) {
                $query->setOffset((int)$filter['offset']);
                $query->setLimit((int)$filter['limit']);
            }
            return $query->execute();
        }
        return $query->execute()->count();
    }

    /**
     * @param array<mixed> $settings array
     * @return array<mixed>|QueryResultInterface
     */
    public function findAllByLocation(array $settings = [])
    {
        $query = $this->createQuery();
        $latitude = 0.0;
        if (isset($settings['latitude'])) {
            $latitude = (float)$settings['latitude'];
        }
        $longitude = 0.0;
        if (isset($settings['longitude'])) {
            $longitude = (float)$settings['longitude'];
        }
        // Search distance
        $distance = isset($settings['distance']) ? (int)$settings['distance'] : 50;
        $offset = isset($settings['offset']) ? (int)$settings['offset'] : 0;
        $limit = isset($settings['limit']) ? (int)$settings['limit'] : 9999;
        // To search by kilometers instead of miles, replace 3959 with 6371.
        $distanceUnit = 6371;
        if ((bool)$settings['inMiles']) {
            $distanceUnit = 3959;
        }
        // Page uid array
        $pageWhere = '';
        if (isset($settings['pageUids']) && is_array($settings['pageUids'])) {
            $validatedPageUids = [];
            if (count($settings['pageUids']) > 0) {
                foreach ($settings['pageUids'] as $pageUid) {
                    $validatedPageUids[] = (int)$pageUid;
                }
                $pageWhere = ' AND pid IN (' . implode(',', $validatedPageUids) . ') ';
            }
        }
        // Build statement
        $sql = 'SELECT ';
        $sql .= '*, (' . $distanceUnit . ' * acos( cos( radians(' . $latitude . ') ) * cos( radians(`latitude`) ) * ';
        $sql .= 'cos( radians(`longitude`) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * ';
        $sql .= 'sin( radians(`latitude`) ) ) ) AS distance FROM tx_portfolios_domain_model_portfolio ';
        $sql .= 'HAVING distance < ' . $distance . $pageWhere . ' ORDER BY distance LIMIT ' . $offset . ', ' . $limit;
        /** @phpstan-ignore-next-line  */
        $query->statement($sql);
        return $query->execute();
    }
}
