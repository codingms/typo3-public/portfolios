<?php

namespace CodingMs\Portfolios\DataProcessing;

use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

class AddPortfolioToMenuProcessor  implements DataProcessorInterface
{

    /**
     * @param ContentObjectRenderer $cObj
     * @param array<mixed> $contentObjectConfiguration
     * @param array<mixed> $processorConfiguration
     * @param array<mixed> $processedData
     * @return array<mixed>
     */
    public function process(ContentObjectRenderer $cObj, array $contentObjectConfiguration, array $processorConfiguration, array $processedData): array
    {
        if (!$processorConfiguration['menus']) {
            return $processedData;
        }
        $portfolioRecord = $this->getPortfolioRecord();
        if ($portfolioRecord) {
            $menus = GeneralUtility::trimExplode(',', $processorConfiguration['menus'], true);
            foreach ($menus as $menu) {
                if (isset($processedData[$menu])) {
                    $this->addPortfolioRecordToMenu($portfolioRecord, $processedData[$menu]);
                }
            }
        }
        return $processedData;
    }

    /**
     * Add the portfolio record to the menu items
     *
     * @param array<string, mixed> $portfolioRecord
     * @param array<int, mixed> $menu
     *
     * @return void
     */
    protected function addPortfolioRecordToMenu(array $portfolioRecord, array &$menu): void
    {
        foreach ($menu as &$menuItem) {
            $menuItem['current'] = 0;
        }

        $menu[] = [
            'data' => $portfolioRecord,
            'title' => $portfolioRecord['title'],
            'active' => 1,
            'current' => 1,
            'link' => GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL'),
            'isPortfolio' => true
        ];
    }

    /**
     * Get the portfolio record including possible translations
     *
     * @return array<mixed>
     * @throws \Doctrine\DBAL\Exception
     */
    protected function getPortfolioRecord(): array
    {
        $portfolioId = 0;
        $vars = GeneralUtility::_GET('tx_portfolios_list');
        if (isset($vars['portfolio'])) {
            $portfolioId = (int)$vars['portfolio'];
        }

        if ($portfolioId) {
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_portfolios_domain_model_portfolio');
            $row = $queryBuilder
                ->select('*')
                ->from('tx_portfolios_domain_model_portfolio')
                ->where(
                    $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($portfolioId, \PDO::PARAM_INT))
                )
                ->executeQuery()
                ->fetchOne();

            if ($row) {
                /** @phpstan-ignore-next-line  */
                $row = $this->getTsfe()->sys_page->getRecordOverlay('tx_portfolios_domain_model_portfolio', $row, $this->getCurrentLanguage());
            }

            if (is_array($row) && !empty($row)) {
                return $row;
            }
        }
        return [];
    }

    /**
     * Get current language
     *
     * @return int
     */
    protected function getCurrentLanguage(): int
    {
        $languageId = 0;
        $context = GeneralUtility::makeInstance(Context::class);
        try {
            $languageId = $context->getPropertyFromAspect('language', 'contentId');
        } catch (AspectNotFoundException $e) {
            // do nothing
        }

        return (int)$languageId;
    }

    /**
     * @return TypoScriptFrontendController
     */
    protected function getTsfe(): TypoScriptFrontendController
    {
        return $GLOBALS['TSFE'];
    }
}
