<?php

declare(strict_types=1);

namespace CodingMs\Portfolios\Tca;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Tca\Configuration as ConfigurationDefaults;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;

/**
 * Configuration presets for TCA fields.
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class Configuration extends ConfigurationDefaults
{

    /**
     * @param string $type
     * @param bool $required
     * @param bool $readonly
     * @param string $label
     * @param array<mixed> $options
     * @return array<string, mixed>
     */
    public static function get(string $type, bool $required = false, bool $readonly = false, string $label = '', array $options = []): array
    {
        switch ($type) {
            case 'portfolioTagVariant':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'default' => 'default',
                    'items' => [
                        ['label' => 'default', 'value' => 'default'],
                        ['label' => 'primary', 'value' => 'primary'],
                        ['label' => 'secondary', 'value' => 'secondary'],
                        ['label' => 'success', 'value' => 'success'],
                        ['label' => 'info', 'value' => 'info'],
                        ['label' => 'warning', 'value' => 'warning'],
                        ['label' => 'danger', 'value' => 'danger'],
                    ],
                ];
                if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
                    $config['items'] = [
                        ['default', 'default'],
                        ['primary', 'primary'],
                        ['secondary', 'secondary'],
                        ['success', 'success'],
                        ['info', 'info'],
                        ['warning', 'warning'],
                        ['danger', 'danger'],
                    ];
                }
                break;
            case 'portfolioTagCategory':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'foreign_table' => 'tx_portfolios_domain_model_portfoliotagcategory',
                    'foreign_table_where' => 'AND tx_portfolios_domain_model_portfoliotagcategory.pid=###CURRENT_PID### AND tx_portfolios_domain_model_portfoliotagcategory.sys_language_uid < 1 ORDER BY title',
                    // Insert empty value
                    'items' => [
                        ['label' => '', 'value' => 0],
                    ],
                    'size' => 1,
                    'maxitems' => 1,
                    'minitems' => 0,
                    'wizards' => [
                        'suggest' => [
                            'type' => 'suggest',
                        ],
                    ],
                ];
                if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
                    $config['items'] = [
                        ['', 0],
                    ];
                }
                break;
            default:
                $config = parent::get($type, $required, $readonly, $label, $options);
                break;
        }
        if ($readonly) {
            $config['readOnly'] = true;
        }
        return $config;
    }
}
