<?php

namespace CodingMs\Portfolios\Controller;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Portfolios\Domain\Model\Portfolio;
use CodingMs\Portfolios\Domain\Repository\PortfolioRepository;
use CodingMs\Portfolios\Domain\Session\SessionHandler;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * JsonApiController
 */
class JsonApiController extends ActionController
{

    /**
     * @var array<int|string, mixed>
     */
    protected array $json = ['messages' => []];

    /**
     * @var PortfolioRepository
     */
    protected PortfolioRepository $portfolioRepository;

    /**
     * @var SessionHandler
     */
    protected SessionHandler $sessionHandler;

    /**
     * @var array<mixed>
     */
    protected array $session = [];

    public function __construct() {
        $this->portfolioRepository = GeneralUtility::makeInstance(PortfolioRepository::class);
        $this->sessionHandler = GeneralUtility::makeInstance(SessionHandler::class);
    }

    /**
     * Remind filter settings
     *
     * @return ResponseInterface
     * @throws \JsonException
     */
    public function filterAction(): ResponseInterface
    {
        // Get content data
        $pid = (int)$GLOBALS['TSFE']->id;
        if ($this->request->hasArgument('remind')) {
            $this->session = $this->sessionHandler->restoreFromSession();
            $this->json['remind'] = ($this->request->getArgument('remind') === 'true');
            if ($this->request->hasArgument('isotope')) {
                $tags = [];
                $tagCategories = [];
                $categories = [];
                $this->json['isotope'] = trim($this->request->getArgument('isotope'));
                if ($this->json['isotope'] !== '*') {
                    $isotopeFilterOr = explode(',', $this->json['isotope']);
                    if (count($isotopeFilterOr) > 0) {
                        foreach ($isotopeFilterOr as $filterOr) {
                            $isotopeFilterAnd = explode('.', $filterOr);
                            if (count($isotopeFilterAnd) > 0) {
                                foreach ($isotopeFilterAnd as $filterAnd) {
                                    $filter = '.' . trim($filterAnd);
                                    if (substr($filter, 0, 15) === '.portfolio-tag-') {
                                        $tagUid = (int)substr($filter, 15);
                                        if ($tagUid > 0) {
                                            $tags[$tagUid] = $tagUid;
                                        }
                                    }
                                    if (substr($filter, 0, 24) === '.portfolio-tag-category-') {
                                        $tagCategoryUid = (int)substr($filter, 24);
                                        if ($tagCategoryUid > 0) {
                                            $tagCategories[$tagCategoryUid] = $tagCategoryUid;
                                        }
                                    }
                                    if (substr($filter, 0, 20) === '.portfolio-category-') {
                                        $categoryUid = (int)substr($filter, 20);
                                        if ($categoryUid > 0) {
                                            $categories[$categoryUid] = $categoryUid;
                                        }
                                    }
                                }
                            }
                        }
                        $tags = array_unique($tags);
                        $tagCategories = array_unique($tagCategories);
                        $categories = array_unique($categories);
                    }
                }
                // Store session data
                $this->session[$pid]['filter']['isotope'] = $this->json['isotope'];
                $this->session[$pid]['filter']['tag']['values'] = $tags;
                $this->session[$pid]['filter']['tagCategory']['values'] = $tagCategories;
                $this->session[$pid]['filter']['category']['values'] = $categories;
            }
            //
            // Remind search word
            if ($this->request->hasArgument('word')) {
                $this->json['word'] = trim($this->request->getArgument('word'));
                $this->session[$pid]['filter']['word']['value'] = $this->json['word'];
            }
            //
            // Write back new filter settings
            $this->sessionHandler->writeToSession($this->session);
            // Return
            $this->json[$pid]['filter']['tag']['values'] = $this->session[$pid]['filter']['tag']['values'];
            $this->json[$pid]['filter']['tagCategory']['values'] = $this->session[$pid]['filter']['tagCategory']['values'];
            $this->json[$pid]['filter']['category']['values'] = $this->session[$pid]['filter']['category']['values'];
            $this->json[$pid]['filter']['word']['value'] = $this->session[$pid]['filter']['word']['value'];
            $this->json[$pid]['filter']['isotope'] = $this->session[$pid]['filter']['isotope'];
        }
        $response = $this->responseFactory->createResponse()
            ->withHeader('Content-Type', 'application/json; charset=utf-8');
        $response->getBody()->write(json_encode($this->json, JSON_THROW_ON_ERROR));

        return $response;
    }

    /**
     * Select data
     *
     * @return ResponseInterface
     * @throws \JsonException
     */
    public function bookmarkAction(): ResponseInterface
    {
        // Restore session data
        $this->session = $this->sessionHandler->restoreFromSession();
        // Be sure the bookmarks structure is available
        if (!isset($this->session['bookmarks'])) {
            $this->session['bookmarks'] = [
                'count' => 0,
                'items' => [],
            ];
        }
        // Remind or forget
        if ($this->request->hasArgument('remind')) {
            $this->json['remind'] = ($this->request->getArgument('remind') === 'true');
            $this->json['portfolio'] = 0;
            // Read portfolio uid
            if ($this->request->hasArgument('portfolio')) {
                $this->json['portfolio'] = (int)$this->request->getArgument('portfolio');
            }
            // Get portfolio object from repository
            $portfolio = $this->portfolioRepository->findByIdentifier($this->json['portfolio']);
            if (!($portfolio instanceof Portfolio)) {
                $translationKey = 'tx_portfolios_label.bookmark_object_not_found';
                $this->json['messages'] = [
                    'error' => $this->translate($translationKey)
                ];
            } else {
                if ($this->json['remind']) {
                    //
                    // Portfolio was found, but is max-item limit reached?!
                    if ($this->session['bookmarks']['count'] < (int)$this->settings['bookmarks']['maxItems']) {
                        $this->session['bookmarks']['items'][$this->json['portfolio']] = $this->json['portfolio'];
                        $translationKey = 'tx_portfolios_label.bookmark_reminded';
                        $arguments = [$portfolio->getTitle()];
                        $this->json['messages'] = [
                            'info' => $this->translate($translationKey, $arguments)
                        ];
                    } else {
                        $translationKey = 'tx_portfolios_label.bookmark_max_items_reached';
                        $arguments = [$portfolio->getTitle()];
                        $this->json['messages'] = [
                            'error' => $this->translate($translationKey, $arguments)
                        ];
                    }
                } else {
                    if (isset($this->session['bookmarks']['items'][$this->json['portfolio']])) {
                        unset($this->session['bookmarks']['items'][$this->json['portfolio']]);
                        $translationKey = 'tx_portfolios_label.bookmark_forgotten';
                        $arguments = [$portfolio->getTitle()];
                        $this->json['messages'] = [
                            'info' => $this->translate($translationKey, $arguments)
                        ];
                    }
                }
            }
        } else {
            if ($this->request->hasArgument('clear')) {
                $this->json['clear'] = ($this->request->getArgument('clear') == 'true');
                if ($this->json['clear']) {
                    $this->session['bookmarks']['items'] = [];
                    $translationKey = 'tx_portfolios_label.bookmarks_cleared';
                    $this->json['messages'] = [
                        'info' => $this->translate($translationKey)
                    ];
                }
            }
        }
        $this->session['bookmarks']['count'] = count($this->session['bookmarks']['items']);
        $this->json['bookmarks'] = $this->session['bookmarks'];
        $this->sessionHandler->writeToSession($this->session);

        $response = $this->responseFactory->createResponse()
            ->withHeader('Content-Type', 'application/json; charset=utf-8');
        $response->getBody()->write(json_encode($this->json, JSON_THROW_ON_ERROR));

        return $response;
    }

    /**
     * @param string $key
     * @param array<mixed> $arguments
     * @return string|null
     */
    protected function translate(string $key, array $arguments = []): ?string
    {
        return LocalizationUtility::translate($key, 'Portfolios', $arguments);
    }
}
