<?php

namespace CodingMs\Portfolios\Controller;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Portfolios\Domain\Model\FileReference;
use CodingMs\Portfolios\Domain\Model\Portfolio;
use CodingMs\Portfolios\Domain\Model\PortfolioCategory;
use CodingMs\Portfolios\Domain\Model\PortfolioTag;
use CodingMs\Portfolios\Domain\Model\PortfolioTagCategory;
use CodingMs\Portfolios\Domain\Repository\PortfolioCategoryRepository;
use CodingMs\Portfolios\Domain\Repository\PortfolioRepository;
use CodingMs\Portfolios\Domain\Repository\PortfolioTagCategoryRepository;
use CodingMs\Portfolios\Domain\Repository\PortfolioTagRepository;
use CodingMs\Portfolios\Domain\Session\SessionHandler;
use CodingMs\Portfolios\PageTitle\PageTitleProvider;
use CodingMs\Portfolios\Service\GeoLocationService;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\MetaTag\MetaTagManagerInterface;
use TYPO3\CMS\Core\MetaTag\MetaTagManagerRegistry;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Service\ImageService;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Extbase\Http\ForwardResponse;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * Portfolios controller
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class PortfolioController extends ActionController
{

    /**
     * @var array<string, mixed>
     */
    protected array $json = ['messages' => []];

    /**
     * @var PortfolioRepository
     */
    protected PortfolioRepository $portfolioRepository;

    /**
     * @var SessionHandler
     */
    protected SessionHandler $sessionHandler;

    /**
     * @var PortfolioCategoryRepository
     */
    protected PortfolioCategoryRepository $portfolioCategoryRepository;

    /**
     * @var PortfolioTagRepository
     */
    protected PortfolioTagRepository $portfolioTagRepository;

    /**
     * @var PortfolioTagCategoryRepository
     */
    protected PortfolioTagCategoryRepository $portfolioTagCategoryRepository;

    /**
     * Selected Category-Object, if there is a selected one
     * @var PortfolioCategory
     */
    protected PortfolioCategory $selectedCategory;

    /**
     * Selected Tag-Object, if there is a selected one
     * @var PortfolioTag
     */
    protected PortfolioTag $selectedTag;

    /**
     * @var GeoLocationService
     */
    protected GeoLocationService $geoLocationService;

    /**
     * @var array<mixed>
     */
    protected array $session = [];

    /**
     * @var array<mixed>
     */
    protected array $content = [];

    public function __construct(
        PortfolioRepository $portfolioRepository,
        PortfolioCategoryRepository $portfolioCategoryRepository,
        PortfolioTagRepository $portfolioTagRepository,
        PortfolioTagCategoryRepository $portfolioTagCategoryRepository,
        GeoLocationService $geoLocationService,
        SessionHandler $sessionHandler
    ) {
        $this->portfolioRepository = $portfolioRepository;
        $this->portfolioCategoryRepository = $portfolioCategoryRepository;
        $this->portfolioTagRepository = $portfolioTagRepository;
        $this->portfolioTagCategoryRepository = $portfolioTagCategoryRepository;
        $this->geoLocationService = $geoLocationService;
        $this->sessionHandler = $sessionHandler;
    }

    /**
     * @return void
     */
    public function initializeAction(): void
    {
        //parent::initializeAction();
        //
        // Validate tag
        // First: Is there are a passed category
        if ($this->request->hasArgument('portfolioTag')) {
            // Save it in Session
            $this->session['selectedTag'] = (int)$this->request->getArgument('portfolioTag');
        } // Otherwise, check if there is a tag setted in settings
        else {
            if (isset($this->settings['showFromTag'])) {
                // Save it in Session
                $this->session['selectedTag'] = (int)$this->settings['showFromTag'];
            }
        }
        // If the selected tag isn't set, initialize it with zero
        if (!isset($this->session['selectedTag'])) {
            $this->session['selectedTag'] = 0;
        } else {
            /** @var PortfolioTag $portfolioTag */
            $portfolioTag = $this->portfolioTagRepository->findByIdentifierIgnoreStoragePage((int)$this->session['selectedTag']);
            $this->selectedTag = $portfolioTag;
        }
        //
        if (!isset($this->session['selectedPortfolio'])) {
            $this->session['selectedPortfolio'] = 0;
        }
        if (!isset($this->session['pdfListQuantity'])) {
            $this->session['pdfListQuantity'] = 0;
        }
    }

    /**
     * @return void
     * @throws InvalidQueryException
     */
    protected function prepareAction(): void
    {
        // Restore session data
        $this->session = $this->sessionHandler->restoreFromSession();
        // Static template available?
        // Debug extension?
        if (!isset($this->settings['debug']) || !isset($this->settings['pro'])) {
            $this->addFlashMessage(
                'Please include the static template!',
                'Error',
                AbstractMessage::ERROR
            );
        }
        $this->settings['debug'] = isset($this->settings['debug']) && (bool)$this->settings['debug'];
        //
        // Container page uid set?
        if (!isset($this->settings['container']['portfolios']) || (int)$this->settings['container']['portfolios'] == 0) {
            $this->addFlashMessage(
                'Please select a container page uid!',
                'Error',
                AbstractMessage::ERROR
            );
        }
        //
        // Extract item elements
        $this->settings['list']['item']['elements'] = GeneralUtility::trimExplode(
            ',',
            $this->settings['list']['item']['elements'],
            true
        );
        if (count($this->settings['list']['item']['elements']) === 0) {
            $this->addFlashMessage(
                'Please select at least one list item element!',
                'Error',
                AbstractMessage::ERROR
            );
        }
        //
        // Use filter fields
        $this->settings['filter']['fields'] = GeneralUtility::trimExplode(
            ',',
            $this->settings['filter']['fields'],
            true
        );
        //
        // Validate filter and associated list layout
        if (in_array('TagCategorized', $this->settings['filter']['fields'])) {
            if ($this->settings['list']['layout'] !== 'Isotope') {
                $this->addFlashMessage(
                    'Tag categorized filter needs list layout Isotope',
                    'Error',
                    AbstractMessage::ERROR
                );
            } else {
                //
                // Display message about non supported filter
                if (count($this->settings['filter']['fields']) > 2) {
                    $filters = $this->settings['filter']['fields'];
                    if (($key = array_search('TagCategorized', $filters)) !== false) {
                        unset($filters[$key]);
                    }
                    if (($key = array_search('Reset', $filters)) !== false) {
                        unset($filters[$key]);
                    }
                    $message = 'Following filter fields are not supported by list layout Isotope: ';
                    $message .= implode(', ', $filters);
                    $this->addFlashMessage(
                        $message,
                        'Error',
                        AbstractMessage::ERROR
                    );
                }
            }
        }
        //
        // Get content data
        if((int)VersionNumberUtility::getCurrentTypo3Version() > 11) {
            /** @var ContentObjectRenderer $contentObject */
            $contentObject = $this->request->getAttribute('currentContentObject');
            $this->content = $contentObject->data;
        } else {
            $this->content = $this->configurationManager->getContentObject()->data;
        }
        $this->view->assign('content', $this->content);
        $pid = $this->content['pid'];
        //
        // Reset filter?
        $reset = false;
        if ($this->request->hasArgument('reset')) {
            $this->session[$pid]['filter'] = [];
            $reset = true;
        }
        //
        // Validate search word
        if ($this->request->hasArgument('word') && !$reset) {
            $this->session[$pid]['filter']['word']['value'] = trim($this->request->getArgument('word'));
        }
        if (!isset($this->session[$pid]['filter']['word'])) {
            $this->session[$pid]['filter']['word'] = ['value' => ''];
        }
        $this->settings['filter']['word']['value'] = $this->session[$pid]['filter']['word']['value'];
        // Explode query fields
        $this->settings['filter']['word']['queryFields'] = GeneralUtility::trimExplode(
            ',',
            $this->settings['filter']['word']['queryFields'] ?? '',
            true
        );
        //
        // Validate location
        if ($this->request->hasArgument('location') && !$reset) {
            $this->session[$pid]['filter']['location']['value'] = trim($this->request->getArgument('location'));
        }
        if (!isset($this->session[$pid]['filter']['location'])) {
            $this->session[$pid]['filter']['location'] = ['value' => ''];
        }
        $this->settings['filter']['location']['value'] = $this->session[$pid]['filter']['location']['value'];
        //
        // Portfolio Category
        //
        // settings.filter.category.allowed:
        // Allowed categories are defined by FlexForm or by available categories in storage
        // This is an array of category uids
        //
        // settings.filter.category.options:
        // Collected categories which are passed to the select box/checkboxes
        // Finally these are the allowed categories.
        // This is an ObjectStorage of Category Model
        //
        // settings.filter.category.values:
        // Array with selected Category Model
        //
        // Preparation
        if (!isset($this->session[$pid]['filter']['category'])) {
            $this->session[$pid]['filter']['category'] = [
                'values' => []
            ];
        }
        //
        // First of all, transfer possible selected categories from FlexForm into allowed
        $this->settings['filter']['category']['allowed'] = GeneralUtility::trimExplode(
            ',',
            $this->settings['filter']['category']['values'],
            true
        );
        // Then, find all categories depending on allowed categories.
        // This might be all available categories in storage (in case of no categories preselected in FlexForm),
        // or exactly the same as selected in FlexForm
        $this->settings['filter']['category']['options'] = $this->portfolioCategoryRepository->findAllOrderedBy(
            $this->settings['filter']['category']['allowed'],
            $this->settings['filter']['category']['sorting'] ?? 'title'
        );
        //
        // First: Is there are a passed category
        $multipleCategoryPassed = false;
        foreach ($this->request->getArguments() as $argumentKey => $argumentValue) {
            if (substr($argumentKey, 0, 9) === 'category-' && trim($argumentValue) !== '' && !$reset) {
                $multipleCategoryPassed = true;
            }
        }
        if ($multipleCategoryPassed) {
            $this->session[$pid]['filter']['category']['values'] = [];
            // Are there multiple categories selected
            foreach ($this->request->getArguments() as $argumentKey => $argumentValue) {
                if (substr($argumentKey, 0, 9) === 'category-' && !$reset) {
                    $categoryUid = (int)$argumentValue;
                    // But the category must be allowed
                    if (count($this->settings['filter']['category']['options']) > 0 && $categoryUid > 0) {
                        /** @var PortfolioCategory $option */
                        foreach ($this->settings['filter']['category']['options'] as $option) {
                            if ($option->getUid() === $categoryUid) {
                                $this->session[$pid]['filter']['category']['values'][$categoryUid] = $option->getUid();
                                $this->settings['filter']['category']['values'][$categoryUid] = $option->getUid();
                            }
                        }
                    }
                }
            }
            $this->settings['filter']['category']['values'] = $this->session[$pid]['filter']['category']['values'];
        } elseif ($this->request->hasArgument('category') && !$reset) {
            $categoryUid = (int)$this->request->getArgument('category');
            // But the category must be allowed
            if (count($this->settings['filter']['category']['options']) > 0) {
                /** @var PortfolioCategory $option */
                foreach ($this->settings['filter']['category']['options'] as $option) {
                    if ($option->getUid() === $categoryUid) {
                        $this->session[$pid]['filter']['category']['values'] = [$categoryUid => $categoryUid];
                        $this->settings['filter']['category']['values'] = [$categoryUid => $categoryUid];
                        $this->settings['filter']['category']['value'] = $categoryUid;
                    }
                }
            }
        }
        //
        // When there were no category passed by parameter,
        // try to restore from session.
        //
        // Difference between settings and session:
        // -> session contains only uids
        // -> settings the real model objects
        // -> session is additionally divided by page
        elseif (count($this->session[$pid]['filter']['category']['values']) > 0) {
            $this->settings['filter']['category']['values'] = [];
            foreach ($this->session[$pid]['filter']['category']['values'] as $categoryUid) {
                /** @var PortfolioCategory $category */
                $category = $this->portfolioCategoryRepository->findByIdentifier($categoryUid);
                // But the category must be allowed
                if (count($this->settings['filter']['category']['options']) > 0) {
                    /** @var PortfolioCategory $option */
                    foreach ($this->settings['filter']['category']['options'] as $option) {
                        if ($option->getUid() === $category->getUid()) {
                            $this->settings['filter']['category']['values'][$category->getUid()] = $category->getUid();
                        }
                    }
                }
            }
        }
        //
        // If the values are still a string, the defaults from plugin must be used!
        if (is_string($this->settings['filter']['category']['values'])) {
            $this->settings['filter']['category']['values'] = $this->settings['filter']['category']['allowed'];
        }
        //
        // Portfolio Tag
        //
        // settings.filter.tag.allowed:
        // Allowed tags are defined by FlexForm or by available tags in storage
        // This is an array of tag uids
        //
        // settings.filter.tag.options:
        // Collected tags which are passed to the select box/checkboxes
        // Finally these are the allowed tags.
        // This is an ObjectStorage of Tag Model
        //
        // settings.filter.tag.values:
        // Array with selected Tag Model
        //
        // Preparation
        if (!isset($this->session[$pid]['filter']['tag'])) {
            $this->session[$pid]['filter']['tag'] = [
                'values' => []
            ];
        }
        //
        // First of all, transfer possible selected tags from FlexForm into allowed
        $this->settings['filter']['tag']['allowed'] = GeneralUtility::trimExplode(
            ',',
            $this->settings['filter']['tag']['values'],
            true
        );
        // Then, find all tags depending of allowed tags.
        // This might be all available tags in storage (in case of no tags preselected in FlexForm),
        // or exactly the same as selected in FlexForm
        $this->settings['filter']['tag']['options'] = $this->portfolioTagRepository->findAllOrderedBy(
            $this->settings['filter']['tag']['allowed'],
            $this->settings['filter']['tag']['sorting'] ?? 'title'
        );
        //
        // First: Is there are a passed tag
        $multipleTagPassed = false;
        foreach ($this->request->getArguments() as $argumentKey => $argumentValue) {
            if (substr($argumentKey, 0, 4) === 'tag-' && trim($argumentValue) !== '' && !$reset) {
                $multipleTagPassed = true;
            }
        }
        if ($multipleTagPassed) {
            $this->session[$pid]['filter']['tag']['values'] = [];
            // Are there multiple tags selected
            foreach ($this->request->getArguments() as $argumentKey => $argumentValue) {
                if (substr($argumentKey, 0, 4) === 'tag-' && !$reset) {
                    $tagUid = (int)$argumentValue;
                    // But the tag must be allowed
                    if (count($this->settings['filter']['tag']['options']) > 0 && $tagUid > 0) {
                        /** @var PortfolioTag $option */
                        foreach ($this->settings['filter']['tag']['options'] as $option) {
                            if ($option->getUid() === $tagUid) {
                                $this->session[$pid]['filter']['tag']['values'][$tagUid] = $option->getUid();
                            }
                        }
                    }
                }
            }
            $this->settings['filter']['tag']['values'] = $this->session[$pid]['filter']['tag']['values'];
            $this->settings['filter']['tag']['preparedBy'] = 'multiplePassedTag';
        } elseif ($this->request->hasArgument('tag') && !$reset) {
            $this->settings['filter']['tag']['preparedBy'] = 'singlePassedTag';
            $tagUid = (int)$this->request->getArgument('tag');
            // But the tag must be allowed
            if (count($this->settings['filter']['tag']['options']) > 0) {
                /** @var PortfolioTag $option */
                foreach ($this->settings['filter']['tag']['options'] as $option) {
                    if ($option->getUid() === $tagUid) {
                        $this->session[$pid]['filter']['tag']['values'] = [$tagUid => $tagUid];
                        $this->settings['filter']['tag']['values'] = [$tagUid => $tagUid];
                        $this->settings['filter']['tag']['value'] = $tagUid;
                    }
                }
            }
        }
        //
        // When there were no tag passed by parameter,
        // try to restore from session.
        //
        // Difference between settings and session:
        // -> session contains only uids
        // -> settings the real model objects
        // -> session is additionally divided by page
        elseif (count($this->session[$pid]['filter']['tag']['values']) > 0) {
            $this->settings['filter']['tag']['values'] = [];
            $this->settings['filter']['tag']['preparedBy'] = 'restoredFromSession';
            foreach ($this->session[$pid]['filter']['tag']['values'] as $tagUid) {
                /** @var PortfolioTag $tag */
                $tag = $this->portfolioTagRepository->findByIdentifier($tagUid);
                // But the tag must be allowed
                if (count($this->settings['filter']['tag']['options']) > 0) {
                    /** @var PortfolioTag $option */
                    foreach ($this->settings['filter']['tag']['options'] as $option) {
                        if ($option->getUid() === $tag->getUid()) {
                            $this->settings['filter']['tag']['values'][$tag->getUid()] = $tag->getUid();
                        }
                    }
                }
            }
        }
        //
        // If the values are still a string, the defaults from plugin must be used!
        if (is_string($this->settings['filter']['tag']['values'])) {
            $this->settings['filter']['tag']['values'] = $this->settings['filter']['tag']['allowed'];
        }
        //
        // Portfolio Tag Category
        //
        // settings.filter.tagCategory.allowed:
        // Allowed tag categories are defined by FlexForm or by available tag categories in storage
        // This is an array of tag uids
        //
        // settings.filter.tag.options:
        // Collected tag categories which are passed to the select box/checkboxes
        // Finally these are the allowed tags.
        // This is an ObjectStorage of Tag Model
        //
        // settings.filter.tag.values:
        // Array with selected Tag Model
        //
        // Preparation
        if (!isset($this->session[$pid]['filter']['tagCategory'])) {
            $this->session[$pid]['filter']['tagCategory'] = [
                'values' => []
            ];
        }
        //
        // First of all, transfer possible selected tags from FlexForm into allowed
        $this->settings['filter']['tagCategory']['allowed'] = GeneralUtility::trimExplode(
            ',',
            $this->settings['filter']['tagCategory']['values'] ?? '',
            true
        );
        // Then, find all tag categories depending on allowed tag categories.
        // This might be all available tag categories in storage (in case of no tag categories preselected in FlexForm),
        // or exactly the same as selected in FlexForm
        $this->settings['filter']['tagCategory']['options'] = $this->portfolioTagCategoryRepository->findAllOrderedBy(
            $this->settings['filter']['tagCategory']['allowed'],
            $this->settings['filter']['tagCategory']['sorting'] ?? 'title'
        );
        //
        // First: Is there are a passed tag category
        if ($this->request->hasArgument('tagCategory')) {
            $tagCategoryUid = (int)$this->request->getArgument('tagCategory');
            // But the tag category must be allowed
            if (count($this->settings['filter']['tagCategory']['options']) > 0) {
                /** @var PortfolioTagCategory $option */
                foreach ($this->settings['filter']['tagCategory']['options'] as $option) {
                    if ($option->getUid() === $tagCategoryUid) {
                        $this->session[$pid]['filter']['tagCategory']['values'] = [
                            $tagCategoryUid => $option
                        ];
                    }
                }
            }
        }
        //
        // When there were no tag category passed by parameter,
        // try to restore from session.
        //
        // Difference between settings and session:
        // -> session contains only uids
        // -> settings the real model objects
        // -> session is additionally divided by page
        elseif (count($this->session[$pid]['filter']['tagCategory']['values']) > 0) {
            $this->settings['filter']['tagCategory']['values'] = [];
            foreach ($this->session[$pid]['filter']['tagCategory']['values'] as $tagCategoryUid) {
                /** @var PortfolioTagCategory $tagCategory */
                $tagCategory = $this->portfolioTagRepository->findByIdentifier($tagCategoryUid);
                // But the tag category must be allowed
                if (count($this->settings['filter']['tagCategory']['options']) > 0) {
                    /** @var PortfolioTagCategory $option */
                    foreach ($this->settings['filter']['tagCategory']['options'] as $option) {
                        if ($option->getUid() === $tagCategory->getUid()) {
                            $this->settings['filter']['tagCategory']['values'][$tagCategory->getUid()] = $tagCategory;
                        }
                    }
                }
            }
        }
        //
        // Sorting
        /**
         * @todo: FE select box should be implemented including session based reminding.
         */
        $availableSortings = GeneralUtility::trimExplode(',', $this->settings['sortFields'], true);
        $this->settings['filter']['sort']['by'] = reset($availableSortings);
        $this->settings['filter']['sort']['order'] = $this->settings['sortOrder'];
        $this->settings['filter']['offset'] = (int)$this->settings['list']['offset'];
        $this->settings['filter']['limit'] = (int)$this->settings['list']['limit'];
        // Write back to session
        $this->sessionHandler->writeToSession($this->session);

        $this->view->assign('settings', $this->settings);
    }

    /**
     * Load portfolios
     *
     * @param array<string, mixed> $filter
     * @return array<int, mixed>|QueryResultInterface
     * @throws InvalidQueryException
     */
    protected function getPortfolios(array $filter = [])
    {
        $filter['preFiltered'] = [];
        //
        // Prepare params for radial search, Currently deactivated because this feature isn't complete
        if (isset($this->settings['filter']['location']['value']) && trim($this->settings['filter']['location']['value']) !== '') {
            $params = [];
            $params['address'] = $this->settings['filter']['location']['value'];
            $params['sensor'] = 'true';
            $geoLocationJson = $this->geoLocationService->getData($params);
            $geoLocation = $this->geoLocationService->getGeoLocationFromData($geoLocationJson);
            // Location params
            if (isset($geoLocation['lat']) && isset($geoLocation['lng'])) {
                $location = [
                    'latitude' => $geoLocation['lat'],
                    'longitude' => $geoLocation['lng'],
                    'pageUids' => [694],
                    'distance' => 100
                ];
                //
                $preFiltered = $this->portfolioRepository->findAllByLocation($location);
                foreach ($preFiltered as $pre) {
                    $filter['preFiltered'][] = $pre->getUid();
                }
            }
        }
        //
        // Get portfolios
        return $this->portfolioRepository->findAllByFilter($filter);
    }

    /**
     * List action
     *
     * @return ResponseInterface
     * @throws InvalidQueryException
     */
    public function listAction(): ResponseInterface
    {
        // Prepare this action
        $this->prepareAction();
        // Get portfolios
        $filter = $this->settings['filter'];
        if ($this->settings['list']['layout'] == 'Isotope') {
            // When Isotope is in use
            // Don't filter by repository, because we need all
            // items for a proper Isotope effect!
            $filter['word']['value'] = '';
            $filter['category']['values'] = [];
            $filter['tag']['values'] = [];
            $filter['tagCategory']['values'] = [];
            /**
             * @todo need to reinitialize with FlexForm values!?
             */
        }
        $portfolios = $this->getPortfolios($filter);
        $this->view->assign('portfolios', $portfolios);
        $this->view->assign('content', $this->content);
        $this->view->assign('settings', $this->settings);
        $this->view->assign('session', $this->session);

        return $this->htmlResponse();
    }

    /**
     * Shows a single portfolio
     *
     * @return ResponseInterface
     * @throws InvalidQueryException
     */
    public function showAction(): ResponseInterface
    {
        // Validate giving portfolio
        $portfolioUid = 0;
        if ($this->request->hasArgument('portfolio')) {
            $portfolioUid = (int)$this->request->getArgument('portfolio');
        }
        $portfolio = $this->portfolioRepository->findByIdentifier($portfolioUid);
        if (!($portfolio instanceof Portfolio)) {
            $this->addFlashMessage('Portfolio not found (' . $portfolioUid . ')', '', AbstractMessage::ERROR);
            return new ForwardResponse('list');
        }
        //
        // Prepare this action
        $this->prepareAction();
        //
        // Get next and previous portfolio for buttons
        $navigation = $this->getPreviousAndNextPortfolio($portfolio);
        //
        // Remind selected Portfolio for navigation
        $this->session['selectedPortfolio'] = $portfolio->getUid();
        //
        // Show related portfolios
        $relatedPortfolioCount = 0;
        $relatedPortfolioSelected = [];
        $relatedPortfoliosFiltered = [];
        if ($this->settings['show']['relatedPortfolios']) {
            // Get selected related portfolios
            $relatedPortfolios = $portfolio->getRelatedPortfolios();
            /** @var Portfolio $relatedPortfolio */
            foreach ($relatedPortfolios as $relatedPortfolio) {
                if ($this->isAllowedRelatedPortfolio($relatedPortfolioSelected, $relatedPortfolio, $portfolio)) {
                    $relatedPortfoliosFiltered[$relatedPortfolioCount] = $relatedPortfolio;
                    $relatedPortfolioCount++;
                    $relatedPortfolioSelected[] = $relatedPortfolio->getUid();
                }
            }

            // Get other portfolios
            $relatedPortfolios = $this->getPortfolios();
            foreach ($relatedPortfolios as $relatedPortfolio) {
                if ($this->isAllowedRelatedPortfolio($relatedPortfolioSelected, $relatedPortfolio, $portfolio)) {
                    $relatedPortfoliosFiltered[$relatedPortfolioCount] = $relatedPortfolio;
                    $relatedPortfolioCount++;
                    $relatedPortfolioSelected[] = $relatedPortfolio->getUid();
                }
            }
        }
        //
        // Validate list page uid
        $this->settings['list']['pageUid'] = (int)$this->settings['list']['pageUid'];
        if ($this->settings['list']['pageUid'] == 0) {
            $this->settings['list']['pageUid'] = $this->content['pid'];
        }
        //
        // Display portfolio
        $this->injectMetaInformation($portfolio, $this->settings['siteName']);
        $this->view->assign('portfolio', $portfolio);
        $this->view->assign('settings', $this->settings);
        $this->view->assign('session', $this->session);
        $this->view->assign('navigation', $navigation);
        $this->view->assign('relatedPortfolios', $relatedPortfoliosFiltered);

        return $this->htmlResponse();
    }

    /**
     * @param array<mixed> $relatedPortfolioSelected
     * @param Portfolio$relatedPortfolio
     * @param Portfolio$portfolio
     * @return bool
     */
    protected function isAllowedRelatedPortfolio(array $relatedPortfolioSelected, Portfolio $relatedPortfolio, Portfolio $portfolio): bool
    {
        $allowed = true;
        //
        // Related can't be the current displayed portfolio!
        if ($relatedPortfolio->getUid() === $portfolio->getUid()) {
            $allowed = false;
        }
        //
        // Don't use multiple selected portfolios
        if (in_array((int)$relatedPortfolio->getUid(), $relatedPortfolioSelected)) {
            $allowed = false;
        }
        //
        // When there are allowed categories
        if (count($this->settings['filter']['category']['allowed']) > 0) {
            $categoryAllowed = false;
            // There must be at least one category available
            foreach ($relatedPortfolio->getCategories() as $relatedCategory) {
                if (in_array($relatedCategory->getUid(), $this->settings['filter']['category']['allowed'])) {
                    $categoryAllowed = true;
                }
            }
            if (!$categoryAllowed) {
                $allowed = false;
            }
        }
        //
        // When there are allowed tags
        if (count($this->settings['filter']['tag']['allowed']) > 0) {
            $tagAllowed = false;
            // There must be at least one category available
            foreach ($relatedPortfolio->getTags() as $relatedTag) {
                if (in_array($relatedTag->getUid(), $this->settings['filter']['tag']['allowed'])) {
                    $tagAllowed = true;
                }
            }
            if (!$tagAllowed) {
                $allowed = false;
            }
        }
        return $allowed;
    }

    /**
     *  Shows next and previous portfolio from current one
     *
     * @param Portfolio $currentPortfolio Current portfolio
     * @return array<string, mixed>
     * @throws InvalidQueryException
     */
    protected function getPreviousAndNextPortfolio(Portfolio $currentPortfolio): array
    {
        $previousPortfolio = null;
        $nextPortfolio = null;
        // Get portfolios
        // For that we use the reminded filter settings!
        /** @var QueryResultInterface $portfolios */
        $portfolios = $this->getPortfolios($this->settings['filter']);
        // Attention: We're moving from upper to lower through the list
        // Moving backwards:
        if ($portfolios->count() > 0) {
            $isFirst = true;
            $beforeCurrent = true;
            /** @var Portfolio $portfolio */
            foreach ($portfolios as $portfolio) {
                if ($this->settings['debug']) {
                    $uid = $portfolio->getUid();
                    $tags = $portfolio->getTagsString();
                    $value = $uid . ', ' . $tags;
                    $title = 'getPreviousAndNextPortfolio: [title: ' . $portfolio->getTitle() . ']';
                    $title .= '[attribute2: ' . $portfolio->getAttribute2() . ']';
                }
                // Before the current portfolio
                if ($beforeCurrent) {
                    // ..and this is not the current one
                    if ($portfolio->getUid() !== $currentPortfolio->getUid()) {
                        // remind it as portfolio before
                        $previousPortfolio = $portfolio;
                    } else {
                        $beforeCurrent = false;
                    }
                } else {
                    // Cycle has already passed the current portfolio
                    // ..and this is not the current one
                    if ($portfolio->getUid() != $currentPortfolio->getUid() && $nextPortfolio == null) {
                        // remind it as portfolio next
                        $nextPortfolio = $portfolio;
                    } else {
                        $beforeCurrent = false;
                    }
                }
                if ($isFirst) {
                    $isFirst = false;
                }
            }
        }
        //
        $navigation = [
            'previous' => $previousPortfolio,
            'previousTitle' => ($previousPortfolio) ? $this->translate('tx_portfolios_label.button_previous_portfolio_title', [$previousPortfolio->getTitle()]) : '',
            'current' => $currentPortfolio,
            'currentTitle' => $currentPortfolio->getTitle(),
            'next' => $nextPortfolio,
            'nextTitle' => ($nextPortfolio) ? $this->translate('tx_portfolios_label.button_next_portfolio_title', [$nextPortfolio->getTitle()]) : ''
        ];
        //
        if ($this->settings['debug']) {
            $navigation['count'] = $portfolios->count();
            $navigation['portfolios'] = $portfolios->toArray();
        }
        return $navigation;
    }

    /**
     * @param string $key
     * @param array<mixed> $arguments
     * @return string|null
     */
    protected function translate(string $key, array $arguments = []): ?string
    {
        return LocalizationUtility::translate($key, 'Portfolios', $arguments);
    }

    /**
     * @param Portfolio $portfolio
     * @param string $siteName
     * @return void
     */
    protected function injectMetaInformation(Portfolio $portfolio, string $siteName = ''): void
    {
        //
        // Get fallback values
        $htmlTitle = trim($portfolio->getHtmlTitle());
        if ($htmlTitle === '') {
            $htmlTitle = $portfolio->getTitle();
        }
        $description = trim($portfolio->getMetaDescription());
        if ($description === '') {
            $description = substr(strip_tags($portfolio->getTeaser()), 0, 160);
        }
        if ($description === '') {
            $description = substr(strip_tags($portfolio->getDescription()), 0, 160);
        }
        //
        /** @var MetaTagManagerRegistry $metaTagManagerRegistry */
        $metaTagManagerRegistry = GeneralUtility::makeInstance(MetaTagManagerRegistry::class);
        //
        /** @var MetaTagManagerInterface $metaTagManagerDescription */
        $metaTagManagerDescription = $metaTagManagerRegistry->getManagerForProperty('description');
        $metaTagManagerDescription->addProperty('description', $description);
        //
        /** @var MetaTagManagerInterface $metaTagManagerAbstract */
        $metaTagManagerAbstract = $metaTagManagerRegistry->getManagerForProperty('abstract');
        $metaTagManagerAbstract->addProperty('abstract', $portfolio->getMetaAbstract());
        //
        /** @var MetaTagManagerInterface $metaTagManagerKeywords */
        $metaTagManagerKeywords = $metaTagManagerRegistry->getManagerForProperty('keywords');
        $metaTagManagerKeywords->addProperty('keywords', $portfolio->getMetaKeywords());
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgTitle */
        $metaTagManagerOgTitle = $metaTagManagerRegistry->getManagerForProperty('og:title');
        $metaTagManagerOgTitle->addProperty('og:title', $htmlTitle);
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgDescription */
        $metaTagManagerOgDescription = $metaTagManagerRegistry->getManagerForProperty('og:description');
        $metaTagManagerOgDescription->addProperty('og:description', $description);
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgType */
        $metaTagManagerOgType = $metaTagManagerRegistry->getManagerForProperty('og:type');
        $metaTagManagerOgType->addProperty('og:type', 'WebPage');
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgSiteName */
        $metaTagManagerOgSiteName = $metaTagManagerRegistry->getManagerForProperty('og:site_name');
        $metaTagManagerOgSiteName->addProperty('og:site_name', $siteName);
        if ($siteName === '{$themes.configuration.siteName}') {
            $messageBody = 'Please set TypoScript constant {$themes.configuration.siteName} with sitename for og:site_name';
            $messageTitle = 'Error';
            $this->addFlashMessage($messageBody, $messageTitle, AbstractMessage::ERROR);
        }
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgUrl */
        $metaTagManagerOgUrl = $metaTagManagerRegistry->getManagerForProperty('og:url');
        $metaTagManagerOgUrl->addProperty('og:url', GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL'));
        //
        // Preview image
        $previewImages = $portfolio->getImages()->toArray();
        if (count($previewImages) > 0) {
            /** @var FileReference $previewImage */
            $previewImage = $previewImages[0];
            /** @var ImageService $imageService */
            $imageService = GeneralUtility::makeInstance(ImageService::class);
            $image = $imageService->getImage($previewImage, $previewImage, true);
            //
            // Cropping
            if ($image->hasProperty('crop') && $image->getProperty('crop')) {
                $cropString = $image->getProperty('crop');
            }
            $cropVariantCollection = CropVariantCollection::create((string)($cropString ?? ''));
            $cropVariant = 'default';
            $cropArea = $cropVariantCollection->getCropArea($cropVariant);
            $processingInstructions = [
                'width' => 600,
                'crop' => $cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($image),
            ];
            //
            $processedImage = $imageService->applyProcessingInstructions($image, $processingInstructions);
            $imageUrl = $imageService->getImageUri($processedImage, true);
            //
            /** @var MetaTagManagerInterface $metaTagManagerOgImage */
            $metaTagManagerOgImage = $metaTagManagerRegistry->getManagerForProperty('og:image');
            $metaTagManagerOgImage->addProperty('og:image', $imageUrl);
        }
        //
        /** @var PageTitleProvider $seoTitlePageTitleProvider */
        $seoTitlePageTitleProvider = GeneralUtility::makeInstance(PageTitleProvider::class);
        $seoTitlePageTitleProvider->setTitle($htmlTitle);
    }
}
