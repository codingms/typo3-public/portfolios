# Portfolios for TYPO3 Change-Log

*	[TASK] Migrate TypoScript imports



## 2023-12-21 Release of version 4.1.6

*	[BUGFIX] Fix undefined method getMetaKeywords



## 2023-12-21 Release of version 4.1.5

*	[BUGFIX] Fix image domain model getter and insert more traits
*	[TASK] Optimize version conditions in PHP code
*	[BUGFIX] Fix code style
*	[BUGFIX] Remove premium tag from basic extension



## 2023-11-07 Release of version 4.1.4

*	[BUGFIX] Fix PHP 7.4 support



## 2023-11-01 Release of version 4.1.3

*	[TASK] Clean up documentation



## 2023-10-25 Release of version 4.1.2

*	[BUGFIX] Fix get content object for TYPO3 11



## 2023-10-16 Release of version 4.1.1

*	[BUGFIX] Fix request TypoScript condition with additional object check



## 2023-10-04 Release of version 4.1.0

*	[TASK] Migrate TCA for TYPO3 11 and 12 and optimize TypoScript constants groups
*	[FEATURE] Add functions for backend module lists, see #9
*	[TASK] Migrate HTTP header for AJAX calls



## 2023-09-06 Release of version 4.0.0

*	[TASK] Add TypoScript constants, see #11
*	[TASK] Refactor controller actions
*	[BUGFIX] Fix ajax methods for JsonApiController
*	[TASK] Migrate to TYPO3 12 and remove support for TYPO3 10
*	[TASK] Preparations for TYPO3 12 migration
*	[TASK] Create TypoScript constants documentation, see #6



## 2023-03-29  Release of version 3.1.1

*	[BUGFIX] Fix accessing undefined array keys in PHP 8+
*	[TASK] CSH language files  migration
*	[TASK] Add TCA migrations
*	[BUGFIX] Fix missing icons in TCA definitions
*	[BUGFIX] Migrate TypoScript conditions in tsconfig



## 2022-08-18  Release of version 3.1.0

*	[FEATURE] Menu processor
*	[TASK] Remove old plugin definition
*	[TASK] Optimize TypoScript conditions


## 2022-04-23  Release of version 3.0.0

*	[TASK] Optimize code style
*	[TASK] Optimize and normalize documentation and configuration
*	[TASK] Remove objectmanager usages
*	[TASK] Add german translation files
*	[TASK] Migration for TYPO3 11 and PHP 8
*	[TASK] Preparation for TYPO3 11 and PHP 8
*	[BUGFIX] Fix relatedPortfoliosLimit condition in Fluid template
*	[BUGFIX] Fix documentation configuration
*	[TASK] Add documentations configuration
*	[TASK] Add sitemap xml documentation
*	[BUGFIX] Fix translations in bookmarks plugin flexform



## 2021-04-30  Release of version 2.1.3

*	[BUGFIX] Fix extension key in composer.json



## 2021-04-26  Release of version 2.1.2

*	[BUGFIX] Fix sorting of Portfolios by sorting field
*	[BUGFIX] Fix white space in getDescriptionForPdf method
*	[BUGFIX] Fix Tag filter for multi-select queries
*	[BUGFIX] Fix page uid for detail page in portfolio items



## 2020-11-10  Release of version 2.1.1

*	[BUGFIX] Fix a tag/tag category loading issue in categorized tag filter
*	[TASK] Switch Isotope filter request to post method
*	[TASK] Add validation for list configuration in order to avoid invalid settings
*	[BUGFIX] Fix tag and category single selection - using integer uids only!
*	[TASK] Clean up documentation files



## 2020-11-04  Release of version 2.1.0

*	[FEATURE] Add slug field in Portfolio record



## 2020-10-28  Release of version 2.0.0

*	[FEATURE] Add image in tag record
*	[BUGFIX] Fix backend module filtering
*	[TASK] Migrate domain model mapping for TYPO3 10
*	[TASK] Optimize error handling if static template is missed
*	[FEATURE] Insert Meta and OG tags in detail view
*	[TASK] Remove Realurl hook
*	[TASK] Remove usage of $_EXTKEY
*	[TASK] Migration usage of $TCA
*	[TASK] Remove inject annotations
*	[TASK] Remove DebugService
*	[TASK] Remove BackendLoginExists ViewHelper, because such a ViewHelper is now provided by TYPO3 core
*	[TASK] Remove MetaTag and TitleTag ViewHelper
*	[TASK] Migrate annotations for TYPO3 9-10



## 2019-10-13  Release of version 1.4.0

*	[TASK] Add Gitlab-CI configuration.
*	[TASK] Remove DEV identifier.
*	[FEATURE] Add reset/all button feature for isotope usage.
*	[BUGFIX] Fixing filtering by a single category select box.
*	[TASK] Adding icon factory icons.
*	[TASK] Cleaning up page TypoScript.
*	[TASK] Extending debugging information.
*	[TASK] Portfolios images script processes active classes for arrow buttons.
*	[TASK] Adding attribute 7 and 8.
*	[BUGFIX] Fixing th initial selected category and tag in plugin.
*	[FEATURE] Adding a query type TypoScript setting, which switches between different query types (AND/OR concatenations).
*	[FEATURE] Adding a new sorting option for attribute 3.
*	[FEATURE] Adding a new query type 3, which sorts the tags by using the tag category.
*	[BUGFIX] Fixing multiple filter AJAX requests, which ends up in a time race.
*	[TASK] Insert jQuery event for Isotope initialization.
*	[BUGFIX] Disabling caching the detail view, because of the next/previous buttons.
*	[BUGFIX] Fixing file translations in Portfolios-Object.
*	[BUGFIX] Fixing sorting issue in portfolios repository.
*	[TASK] Ignore chash in this extension.
*	[TASK] Adding title for link tags.
*	[BUGFIX] Fixing session issue in single view.



## 2018-01-23  Release of version 1.3.0

*	[FEATURE] Adding setting for selecting the sorting of Tags, Categories and TagCategories.
*	[FEATURE] Passing FlexForm-Sorting into Repository method.
*	[TASK] Optimizing debugging logic and setting.



## 2018-01-12  Release of version 1.2.1

*	[BUGFIX] Fixing fetch translated portfolios.



## 2017-11-16  Release of version 1.2.0

*	[FEATURE] Adding an InArray-ViewHelper
*	[FEATURE] Adding logic for receiving multiple categories by parameter
*	[FEATURE] Adding attribute 6 on Portfolios model for tackling with custom attributes
*	[BUGFIX] Changing Portfolio model query from logicalAnd to logicalOr
*	[FEATURE] Passing search field values by TypoScript
*	[FEATURE] Extending JSON-API so that the search word is reminded too
*	[FEATURE] Adding a preparation of a radial search for portfolios



## 2017-11-16  Release of version 1.1.0

*	[FEATURE] Adding a highlight flag for portfolios
*	[FEATURE] Adding a files/downloads for portfolios
*	[FEATURE] Adding a field for MarkDown text
*	[TASK] Removing exclude fields from TCA and FlexForm
*	[TASK] Adding some Page-TypoScript for configuring the backend
*	[BUGFIX] Adding getter and setter in Tag model
*	[FEATURE] Image assignment in reference tag
*	[TASK] Modifying constants for Themes compatibility
*	[BUGFIX] Related references should not show the shown reference
*	[BUGFIX] Fixing validation of the selected category
*	[TASK] Modify ViewHelper namespace
*	[BUGFIX] Check in Reference Model if finishing date is DateTime
*	[TASK] Integrate a new content element wizard
*	[TASK] Extend constants with translations and constants groups

