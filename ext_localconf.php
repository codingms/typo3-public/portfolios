<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Portfolios',
    'List',
    [\CodingMs\Portfolios\Controller\PortfolioController::class => 'list, show'],
    [\CodingMs\Portfolios\Controller\PortfolioController::class => 'list, show']
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Portfolios',
    'JsonApi',
    [\CodingMs\Portfolios\Controller\JsonApiController::class => 'bookmark, filter'],
    [\CodingMs\Portfolios\Controller\JsonApiController::class => 'bookmark, filter']
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '@import "EXT:portfolios/Configuration/PageTS/tsconfig.typoscript"'
);

//
// register svg icons: identifier and filename
$iconsSvg = [
    'contains-portfolios' => 'Resources/Public/Icons/Extension.svg',
    'apps-pagetree-folder-contains-portfolios' => 'Resources/Public/Icons/Extension.svg',
    'content-plugin-portfolios-list' => 'Resources/Public/Icons/iconmonstr-medal-3.svg',
    'mimetypes-x-content-portfolios-portfolio' => 'Resources/Public/Icons/iconmonstr-medal-3.svg',
    'mimetypes-x-content-portfolios-portfoliocategory' => 'Resources/Public/Icons/iconmonstr-link-2.svg',
    'mimetypes-x-content-portfolios-portfoliotag' => 'Resources/Public/Icons/iconmonstr-tag-4.svg',
    'mimetypes-x-content-portfolios-portfoliotagcategory' => 'Resources/Public/Icons/iconmonstr-tag-4.svg',
];
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
foreach ($iconsSvg as $identifier => $path) {
    $iconRegistry->registerIcon(
        $identifier,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:portfolios/' . $path]
    );
}
