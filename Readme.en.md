# Portfolios Extension for TYPO3

This extension provides categorized portfolios and reference for a website.


**Features:**

*   Displaying of portfolios/references/working examples
*   Categories, tags ans tag-categories available
*   Optimized for better search engine ranking
*   Displaying of related portfolios/references
*   Flexible list and detail view
*   Teaser views possible
*   Adding of many images and downloads in portfolio/reference possible
*   Back and forward buttons in detail view, for a quick navigation to the next portfolio/references
*   Radial search for finding portfolios/references close to the user

**Pro-Features:**

*   List view with complex multi-filter by using Isotope feature
*   PDF generator for generating portfolio/reference PDFs
*   Backend module for comfortable record management
*   Bookmarks feature for portfolios/references
*   Dynamic Bookmarks PDF generation of all bookmarked portfolios/references
*   Cover page and table of contents in bookmark PDF possible

If you need some additional or custom feature - get in contact!


**Links:**

*   [Portfolios Documentation](https://www.coding.ms/documentation/typo3-portfolios "Portfolios Documentation")
*   [Portfolios Bug-Tracker](https://gitlab.com/codingms/typo3-public/portfolios/-/issues "Portfolios Bug-Tracker")
*   [Portfolios Repository](https://gitlab.com/codingms/typo3-public/portfolios "Portfolios Repository")
*   [TYPO3 Portfolios Productdetails](https://www.coding.ms/typo3-extensions/typo3-portfolios/ "TYPO3 Portfolios Productdetails")
*   [TYPO3 Portfolios Documentation](https://www.coding.ms/documentation/typo3-portfolios "TYPO3 Portfolios Documentation")
*   [Demo: Drewes+Speth](https://www.drewes-speth.de/tragwerke "Demo: Drewes+Speth")
*   [Demo: coding.ms](https://www.coding.ms/de/portfolio "Demo: coding.ms")
*   [Demo: Gebo-Therm](https://www.gebotherm.com/unternehmen/referenzen/ "Demo: Gebo-Therm")
