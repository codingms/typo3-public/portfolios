/**
 * Portfolios
 *
 * Thomas Deuling <typo3@coding.ms>
 * 2017-01-14 - Muenster/Germany
 */
var Portfolios = {

	/**
	 * Debugging
	 */
	debug: false,

	/**
	 * JSON API URL
	 */
	jsonApi: '',

	/**
	 * Selected filter
	 */
	filter: {
		tags: []
	},

	/**
	 * Initialize object
	 */
	initialize: function () {
		//
		// Debug script?
		var debug = jQuery('[data-portfolios-debug]');
		if (debug.length === 1 && debug.attr('data-portfolios-debug') === '1') {
			Logger.enableLogger();
			Portfolios.debug = true;
		}
		//
		// Get JSON API url
		var jsonApiContainer = jQuery('[data-portfolios-json-api]');
		if (jsonApiContainer.length === 0) {
			Logger.warn('Portfolios JSON API not found!');
		}
		else {
			Portfolios.jsonApi = jsonApiContainer.attr('data-portfolios-json-api');
		}
		//
		// Initialize bookmarks
		this.bookmarks.initialize();
		//
		//
		jQuery('[data-portfolio-image-preview]').click(function() {
			var preview = jQuery(this);
			var imageNumber = parseInt(preview.attr('data-portfolio-image-preview'), 10);
			Portfolios.details.images.showImageNumber(imageNumber);
		});
		jQuery('[data-portfolio-image-navigation]').click(function() {
			var navigation = jQuery(this);
			var action = navigation.attr('data-portfolio-image-navigation');
			if(action==='previous') {
				Portfolios.details.images.showPrevious();
			}
			else if(action==='next') {
				Portfolios.details.images.showNext();
			}
			return false;
		});
		//
		if(jQuery('.portfolios-show').length > 0) {
			Portfolios.details.images.initialize();
		}
		//
		// Initialize Isotope, if required
		this.isotope.instance = jQuery('.tx-portfolios [data-isotope]');
		if(this.isotope.instance.length>0) {
			Portfolios.isotope.initialize();
		}
	},

	/**
	 * Isotope configuration
	 */
	isotope: {

		/**
		 * Isotope instance, if available
		 */
		instance: null,

		/**
		 * Sort by
		 */
		sortBy: 'title',

		/**
		 * Sort order
		 */
		sortOrder: 'asc',

		/**
		 * Initialize Isotope
		 * @todo Move to PRO
		 */
		initialize: function() {
			Logger.log('Portfolios.isotope.initialize');
			// Portfolio tags
			var tagCheckboxes = jQuery('[data-portfolio-tag] input[type=\'checkbox\']');
			if(tagCheckboxes.length>0) {
				// Restore tag filter settings
				jQuery.each(tagCheckboxes, function() {
					var checkbox = jQuery(this);
					if(checkbox.prop('checked')) {
						var parent = 0;
						var categorized = checkbox.closest('.portfolio-filter-tag-categorized');
						var filterConcatenation = checkbox.attr('data-filter-concatenation');
						Logger.log('Tag checkbox filter concatenation: ', filterConcatenation, ', categorized: ', categorized);
						if(categorized.length>0) {
							var tagCategory = jQuery('.portfolio-tag-category input[type=\'checkbox\']', categorized);
							parent = parseInt(tagCategory.val(), 10);
							tagCategory.prop('checked', true);
						}
						Portfolios.isotope.filter.activate(checkbox.val(), filterConcatenation, parent);
					}
				});
				// On change events
				tagCheckboxes.change(function() {
					var checkbox = jQuery(this);
					var checkboxValue = '.portfolio-tag-' + checkbox.val();
					var portfolio = checkbox.closest('[data-portfolios-version]');
					var parent = 0;
					var categorized = checkbox.closest('.portfolio-filter-tag-categorized');
					var filterConcatenation = checkbox.attr('data-filter-concatenation');
					Logger.log('Tag checkbox filter concatenation: ', filterConcatenation, ', categorized: ', categorized);
					if(categorized.length>0) {
						var tagCategory = jQuery('.portfolio-tag-category input[type=\'checkbox\']', categorized);
						Logger.log('tagCategory', tagCategory);
						parent = parseInt(tagCategory.val(), 10);
						// Activate category checkbox
						if (jQuery('[data-portfolio-tag] input[type=\'checkbox\']:checked', categorized).length > 0) {
							tagCategory.prop('checked', true);
						}
						else {
							tagCategory.prop('checked', false);
						}
					}
					// Checkbox is selected?
					if (checkbox.prop('checked')) {
						Portfolios.isotope.filter.activate(checkbox.val(), filterConcatenation, parent);
					}
					else {
						// Unset tag category
						var category = checkbox.closest('.portfolio-filter-tag-category');
						category = jQuery('[data-portfolio-tag-category] input[type=\'checkbox\']', category);
						// ..but only if available
						if(category.length > 0) {
							// ..and only when checked
							if(category.prop('checked')) {
								category.prop('checked', false);
							}
						}
						// Unset tag
						Portfolios.isotope.filter.deactivate(checkbox.val(), parent);
					}
				});
			}
			// Portfolio tag categories
			var tagCategoryCheckboxes = jQuery('[data-portfolio-tag-category] input[type=\'checkbox\']');
			if(tagCategoryCheckboxes.length>0) {
				// Restore tag filter settings
				jQuery.each(tagCategoryCheckboxes, function() {
					var checkbox = jQuery(this);
					if(checkbox.prop('checked')) {
						Portfolios.isotope.filter.selected.push('.portfolio-tag-category-' + checkbox.val());
					}
				});
				// On change events
				tagCategoryCheckboxes.change(function() {
					var filter = jQuery(this);
					var parent = 0;
					var categorized = filter.closest('.portfolio-filter-tag-categorized');
					if(filter.prop('checked')) {
						// Add checked
						jQuery.each(jQuery('[data-portfolio-tag] input[type=\'checkbox\']', categorized), function() {
							jQuery(this).prop('checked', true).change();
						});
					}
					else {
						// Remove checked
						jQuery.each(jQuery('[data-portfolio-tag] input[type=\'checkbox\']', categorized), function() {
							jQuery(this).prop('checked', false).change();
						});
					}
				});
			}
			// Set filter
			Portfolios.isotope.filter.selected = jQuery.unique(Portfolios.isotope.filter.selected);
			Portfolios.isotope.filter.selectedString = Portfolios.isotope.filter.selected.join(', ');
			// Sort by
			var sortBy = jQuery('#portfolio-list-sort-by');
			if(sortBy.length>0) {
				sortBy.change(Portfolios.isotope.refresh);
			}
			// Sort order
			var sortOrder = jQuery('#portfolio-list-sort-order');
			if(sortOrder.length>0) {
				sortOrder.change(Portfolios.isotope.refresh);
			}
			// Reset/all button
			Portfolios.isotope.filter.resetButton = jQuery('#portfolio-list-filter-reset');
			if(Portfolios.isotope.filter.resetButton.length>0) {
				Portfolios.isotope.filter.resetButton.click(function() {
					Portfolios.isotope.filter.reset();
					event.stopPropagation();
					return false;
				});
				if(Portfolios.isotope.filter.activatedString === '') {
					Portfolios.isotope.filter.resetButton.addClass('active');
				}
				else {
					Portfolios.isotope.filter.resetButton.removeClass('active');
				}
			}
			//
			// Isotope layout position fix
			setTimeout(function() {
				Portfolios.isotope.instance.isotope('layout');
			}, 500);
			//
			// Trigger event
			jQuery.event.trigger({
				type: 'portfolios.isotope.initialized'
			});
		},

		refresh: function() {
			var sortBy = jQuery('#portfolio-list-sort-by');
			var sortOrder = jQuery('#portfolio-list-sort-order');
			Logger.log('Portfolios.isotope.refresh: ', sortBy, sortOrder);
			if(sortOrder.length>0 && sortBy.length>0) {
				Logger.log('Portfolios.isotope.refresh: ', sortBy.val(), ', order asc: ', (sortOrder.val() === 'asc'));
				Portfolios.isotope.instance.isotope({
					filter: Portfolios.isotope.filter.get(),
					getSortData: {
						title: '[data-isotope-sortby-title]',
						portfolio_no: '[data-isotope-sortby-portfolio-no]',
						price:  function(item) {
							return parseFloat(jQuery(item).find(".portfolio-price").attr('data-isotope-sortby-price'));
						},
						sorting:  function(item) {
							return parseInt(jQuery(item).attr('data-isotope-sortby-sorting'), 10);
						}
					},
					sortBy: sortBy.val(),
					sortAscending: (sortOrder.val() === 'asc')
				});
				Portfolios.isotope.instance.isotope('updateSortData').isotope();
			}
			else {
				Portfolios.isotope.instance.isotope({filter: Portfolios.isotope.filter.get()});
			}
		},

		/**
		 * Isotope filter
		 */
		filter: {

			activated: {},
			activatedString: '',
			selected: [],
			selectedString: '',
			resetButton: [],

			/**
			 * Activate a filter setting
			 * @param value
			 * @param concatenation
			 * @param parent
			 */
			activate: function(value, concatenation, parent) {
				Logger.log('Portfolios.isotope.filter.activate: ', value, concatenation, parent);
				if(typeof Portfolios.isotope.filter.activated[parent] === 'undefined') {
					Portfolios.isotope.filter.activated[parent] = {
						concatenation: '',
						items: {}
					}
				}
				Portfolios.isotope.filter.activated[parent].concatenation = concatenation;
				Portfolios.isotope.filter.activated[parent].items[value] = value;
				Portfolios.isotope.refresh();
			},

			/**
			 * Deactivate a filter setting
			 * @param value
			 * @param parent
			 */
			deactivate: function(value, parent) {
				Logger.log('Portfolios.isotope.filter.deactivate: ', value, parent);
				if(typeof Portfolios.isotope.filter.activated[parent] !== 'undefined') {
					delete Portfolios.isotope.filter.activated[parent].items[value];
					if(jQuery.isEmptyObject(Portfolios.isotope.filter.activated[parent].items)) {
						delete Portfolios.isotope.filter.activated[parent];
					}
				}
				Portfolios.isotope.refresh();
			},

			reset: function() {
				Portfolios.isotope.instance.isotope({filter: ''});
				jQuery('.portfolios-list-filter input[type=\'checkbox\']').prop('checked', false);
				Portfolios.isotope.filter.remindInSession('');
			},

			/**
			 * Get current filter settings
			 * @returns {string}
			 */
			get: function() {
				Logger.log('Portfolios.isotope.filter.get: ', Portfolios.isotope.filter);
				Portfolios.isotope.filter.activatedArray = {};
				Portfolios.isotope.filter.activatedString = '';
				var first = true;
				var tempArray = [];
				jQuery.each(Portfolios.isotope.filter.activated, function(key, value) {
					if(value.concatenation === 'and') {
						// Concatenate with AND
						jQuery.each(value.items, function(itemKey) {
							if(typeof Portfolios.isotope.filter.activatedArray[key] === 'undefined') {
								Portfolios.isotope.filter.activatedArray[key] = '.portfolio-tag-' + itemKey;
							}
							else {
								Portfolios.isotope.filter.activatedArray[key] = Portfolios.isotope.filter.activatedArray[key] + '.portfolio-tag-' + itemKey;
							}
						})
					}
					else {
						// Concatenate with OR
						if(first) {
							jQuery.each(value.items, function(itemKey) {
								tempArray[tempArray.length] = '.portfolio-tag-' + itemKey;
							});
							first = false;
						}
						else {
							var newTempArray = [];
							jQuery.each(value.items, function(itemKey) {
								jQuery.each(tempArray, function(tempKey, tempValue) {
									newTempArray[newTempArray.length] = tempValue + '.portfolio-tag-' + itemKey;
								});
							});
							tempArray = newTempArray;
						}
						Portfolios.isotope.filter.activatedArray = tempArray;
					}
				});
				// Map object into array
				Portfolios.isotope.filter.activatedArray = jQuery.map(Portfolios.isotope.filter.activatedArray, function(e) {
					return e;
				});
				// Join array and return
				Portfolios.isotope.filter.activatedString = Portfolios.isotope.filter.activatedArray.join(', ');
				Logger.log('Portfolios.isotope.filter.get: ', Portfolios.isotope.filter.activatedString);
				//
				// Remind isotope filter in session
				if(Portfolios.isotope.filter.remindInSessionStack.length > 0) {
					//
					// In case of running AJAX requests, stack new ones
					// Portfolios.isotope.filter.remindInSessionStack.push(Portfolios.isotope.filter.activatedString);
					//
					// Try to replace only the last one, in order to reduce the requests
					Portfolios.isotope.filter.remindInSessionStack[1] = Portfolios.isotope.filter.activatedString;
				}
				else {
					Portfolios.isotope.filter.remindInSessionStack.push(Portfolios.isotope.filter.activatedString);
					Portfolios.isotope.filter.remindInSession(Portfolios.isotope.filter.activatedString);
				}
				//
				// Active class for reset/all button
				if(Portfolios.isotope.filter.resetButton.length > 0) {
					if(Portfolios.isotope.filter.activatedString === '') {
						Portfolios.isotope.filter.resetButton.addClass('active');
					}
					else {
						Portfolios.isotope.filter.resetButton.removeClass('active');
					}
				}
				return Portfolios.isotope.filter.activatedString;
			},

			remindInSessionStack: [],

			remindInSession: function(activatedString) {
				var data = {
					tx_portfolios_jsonapi: {
						action: 'filter',
						remind: true,
						isotope: activatedString
					}
				};
				Portfolios.isotope.filter.getAjax = jQuery.ajax({
					url: Portfolios.jsonApi,
					dataType: 'json',
					data: data,
					type: 'post',
					success: function (json) {
						Logger.log(json);
						if(Portfolios.isotope.filter.remindInSessionStack.length > 0) {
							// Remove last processed one
							Portfolios.isotope.filter.remindInSessionStack.shift();
							if(Portfolios.isotope.filter.remindInSessionStack.length > 0) {
								// If there are still ones, process them
								Portfolios.isotope.filter.remindInSession(Portfolios.isotope.filter.remindInSessionStack[0]);
							}
						}
					},
					error: function () {
						//
					}
				});
			}

		}

	},

	details: {
		/**
		 * @todo remove that stuff - slides should be realize with slider libraries
		 */
		images: {

			/**
			 * The current selected image
			 */
			currentNumber: 1,

			/**
			 * Initialize images
			 */
			initialize: function () {
				Portfolios.details.images.showImageNumber(1);
			},

			/**
			 * Detail-Event: Click on preview image
			 * @returns {boolean}
			 */
			showImageNumber: function(number) {
				Logger.log('show', number);
				var index = number-1;
				// Image
				var images = jQuery('.portfolio-image');
				if(typeof images[index] !== 'undefined') {
					Portfolios.details.images.currentNumber = number;
					images.removeClass('active');
					jQuery(images[index]).addClass('active');
				}
				// Preview image
				var previews = jQuery('.portfolio-image-preview');
				if(typeof previews[index] !== 'undefined') {
					previews.removeClass('active');
					jQuery(previews[index]).addClass('active');
				}
				// Enable/disable arrow buttons
				var buttonLeft = jQuery('.navigation .left').addClass('active');
				var buttonRight = jQuery('.navigation .right').addClass('active');
				if(index === 0) {
					buttonLeft.removeClass('active');
				}
				if(index === (images.length-1)) {
					buttonRight.removeClass('active');
				}
				return false;
			},

			/**
			 * Show next image
			 * @returns {boolean}
			 */
			showNext: function() {
				var count = jQuery('.portfolio-image').length;
				var number = Portfolios.details.images.currentNumber+1;
				Logger.log('next', number);
				if(number <= count) {
					Portfolios.details.images.showImageNumber(number);
				}
				return false;
			},

			/**
			 * Show previous image
			 * @returns {boolean}
			 */
			showPrevious: function() {
				var number = Portfolios.details.images.currentNumber-1;
				Logger.log('prev', number);
				if(number > 0) {
					Portfolios.details.images.showImageNumber(number);
				}
				return false;
			}
		}
	},

	/**
	 * Check if there are items selected
	 */
	checkQuantity: function () {
		var quantity = parseInt(jQuery('.reference-selected-quantity').html(), 10);
		if (quantity > 0) {
			return true;
		}
		else {
			alert('Bitte wählen Sie zuerst mindestens ein Portfolio aus!');
			return false;
		}
	},

	/**
	 * Bookmark portfolio
	 */
	bookmarks: {

		/**
		 * Initializes the bookmarks
		 */
		initialize: function() {
			// Bind bookmark buttons
			var bookmarkItems = jQuery('[data-portfolio-bookmark-item-uid]');
			if(bookmarkItems.length>0) {
				jQuery.each(bookmarkItems, function() {
					jQuery(this).change(Portfolios.bookmarks.change);
				});
			}
		},

		/**
		 * Clears all reminded bookmarks
		 * @returns {boolean}
		 */
		clear: function() {
			// Clear portfolio
			var data = {
				tx_portfolios_jsonapi: {
					clear: true
				}
			};
			// Send bookmark action
			jQuery.ajax({
				url: Portfolios.jsonApi,
				dataType: 'json',
				data: data,
				type: 'post',
				success: function (json) {
					// Refresh bookmarks count
					var bookmarkButton = jQuery('.bookmarks-button');
					var bookmarkButtonCount = jQuery('.bookmarks-item-count');
					if (bookmarkButtonCount.length > 0) {
						bookmarkButtonCount.html(json.bookmarks.count);
						json.bookmarks.count = parseInt(json.bookmarks.count, 10);
						if(json.bookmarks.count === 0) {
							bookmarkButton.addClass('disabled');
							// Unset all bookmark checkboxes
							jQuery('[data-portfolio-bookmark-item-uid]').prop('checked', false);
							//
							location.reload();
						}
						else {
							bookmarkButton.removeClass('disabled');
						}
					}
					if(typeof json.messages.info !== 'undefined') {
						FlashMessage.push(json.messages.info, 'info', '#portfolio-flash-messages');
					}
				},
				error: function () {
					//
				}
			});
			return false;
		},

		/**
		 * On change bookmarks button
		 * @param portfolio
		 * @returns {boolean}
		 */
		change: function (portfolio) {
			// Default: Forget portfolio
			var data = {
				tx_portfolios_jsonapi: {
					remind: false,
					portfolio: parseInt(jQuery(this).attr('value'), 10)
				}
			};
			// set bookmark
			if (jQuery(this).prop('checked')) {
				data.tx_portfolios_jsonapi.remind = true;
			}
			// Send bookmark action
			jQuery.ajax({
				url: Portfolios.jsonApi,
				dataType: 'json',
				data: data,
				type: 'post',
				success: function (json) {
					if(typeof json.messages.error !== 'undefined') {
						// Some error happen?
						FlashMessage.push(json.messages.error, 'danger', '#portfolio-flash-messages');
						// uncheck checkbox!
						jQuery('#portfolio-bookmark-button_' + json.portfolio).prop('checked', false);
					}
					else {
						// Refresh bookmarks count
						var bookmarkButton = jQuery('.bookmarks-button');
						var bookmarkButtonCount = jQuery('.bookmarks-item-count');
						if (bookmarkButtonCount.length > 0) {
							bookmarkButtonCount.html(json.bookmarks.count);
							json.bookmarks.count = parseInt(json.bookmarks.count, 10);
							if(json.bookmarks.count === 0) {
								bookmarkButton.addClass('disabled').attr('disabled', 'disabled');
							}
							else {
								bookmarkButton.removeClass('disabled').removeAttr('disabled');
							}
						}
						//
						// Ensure that only reminded bookmarks are checked!
						jQuery('[data-portfolio-bookmark-item-uid]').prop('checked', false);
						jQuery.each(json.bookmarks.items, function(uid) {
							jQuery('[data-portfolio-bookmark-item-uid=\'' + uid + '\']').prop('checked', true);
						});
						if(typeof json.messages.info !== 'undefined') {
							FlashMessage.push(json.messages.info, 'info', '#portfolio-flash-messages');
						}
					}
				},
				error: function () {
					//
				}
			});
			return false;
		}
	}
};
jQuery(document).ready(function () {
	Portfolios.initialize();
});
