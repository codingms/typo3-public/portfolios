<?php

declare(strict_types=1);

return [
    \CodingMs\Portfolios\Domain\Model\FileReference::class => [
        'tableName' => 'sys_file_reference',
    ],
    \CodingMs\Portfolios\Domain\Model\Portfolio::class => [
        'tableName' => 'tx_portfolios_domain_model_portfolio',
        'properties' => [
            'creationDate' => [
                'fieldName' => 'crdate'
            ],
            'creationUser' => [
                'fieldName' => 'cruser_id'
            ],
            'modificationDate' => [
                'fieldName' => 'tstamp'
            ],
        ],
    ],
];
