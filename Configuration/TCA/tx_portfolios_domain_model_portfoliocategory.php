<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}
$extKey = 'portfolios';
$table = 'tx_portfolios_domain_model_portfoliocategory';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'sortby' => 'sorting',
        'dividers2tabs' => true,
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,description,image,',
        'iconfile' => 'EXT:portfolios/Resources/Public/Icons/iconmonstr-link-2.svg',
        'typeicon_classes' => [
            'default' => 'mimetypes-x-content-portfolios-portfoliocategory'
        ],
    ],
    'types' => [
        '1' => ['showitem' => '
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_general') . ',
            information,
            title,
            description,
            image,
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_language') . ',
            sys_language_uid;;;;1-1-1,
            l10n_parent,
            l10n_diffsource,
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_access') . ',
            hidden;;1
        '],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'information' => \CodingMs\AdditionalTca\Tca\Configuration::full('information', $table, $extKey),
        'sys_language_uid' => \CodingMs\Portfolios\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\Portfolios\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\Portfolios\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\Portfolios\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\Portfolios\Tca\Configuration::full('hidden'),
        'starttime' => \CodingMs\Portfolios\Tca\Configuration::full('starttime'),
        'endtime' => \CodingMs\Portfolios\Tca\Configuration::full('endtime'),
        'title' => [
            'exclude' => 0,
            'label' => $lll . '.title',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('string', true),
        ],
        'sorting' => [
            'exclude' => 0,
            'label' => $lll . '.sorting',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('int', false, true),
        ],
        'description' => [
            'exclude' => 0,
            'label' => $lll . '.description',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled]',
        ],
        'image' => [
            'exclude' => 0,
            'label' => $lll . '.image',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('imageSingleAltTitle'),
        ],
    ],
];

# Re-add the field for TYPO3 11 and return the $return.
if((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
}
return $return;
