<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

//
// Static template
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'portfolios',
    'Configuration/TypoScript',
    'Portfolios'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'portfolios',
    'Configuration/TypoScript/Stylesheet',
    'Portfolios - Default stylesheets'
);
