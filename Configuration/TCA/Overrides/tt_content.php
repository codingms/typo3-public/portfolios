<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}
//
// List plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'portfolios',
    'List',
    'Portfolio - List (with detail view)'
);
//
// Include flex forms
$flexForms = ['List'];
foreach ($flexForms as $pluginName) {
    $extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase('portfolios');
    $pluginSignature = strtolower($extensionName) . '_' . strtolower($pluginName);
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
    $flexForm = 'FILE:EXT:portfolios/Configuration/FlexForms/' . $pluginName . '.xml';
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, $flexForm);
}
