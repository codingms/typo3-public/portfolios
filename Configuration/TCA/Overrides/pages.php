<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}
//
// Page tree icon
if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [
        0 => 'LLL:EXT:portfolios/Resources/Private/Language/locallang_db.xlf:tx_portfolios_label.contains_portfolios',
        1 => 'portfolios',
        2 => 'apps-pagetree-folder-contains-portfolios'
    ];
} else {
    $GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [
        'label' => 'LLL:EXT:portfolios/Resources/Private/Language/locallang_db.xlf:tx_portfolios_label.contains_portfolios',
        'value' => 'portfolios',
        'icon' => 'apps-pagetree-folder-contains-portfolios'
    ];
}

$GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-portfolios'] = 'apps-pagetree-folder-contains-portfolios';
