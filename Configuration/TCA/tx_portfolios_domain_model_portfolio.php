<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}
$extKey = 'portfolios';
$table = 'tx_portfolios_domain_model_portfolio';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'sortby' => 'sorting',
        'dividers2tabs' => true,
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,sub_title,teaser,description,www,attribute1,attribute2,attribute3,attribute4,attribute5,attribute6,',
        'iconfile' => 'EXT:portfolios/Resources/Public/Icons/iconmonstr-medal-3.svg',
        'typeicon_classes' => [
            'default' => 'mimetypes-x-content-portfolios-portfolio'
        ],
    ],
    'types' => [
        '1' => ['showitem' => '
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_general') . ',
            information,
            title,
            --palette--;;sub_title_highlight,
            slug,
            teaser,
            description,
            finishing_date,
            working_hours,
            www,
            --palette--;' . $lll . '.palette_geo_coordinates;geo_coordinates,
            attribute1,
            attribute2,
            attribute3,
            attribute4,
            attribute5,
            attribute6,
            attribute7,
            attribute8,
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_seo') . ',
            html_title,
            meta_abstract,
            meta_description,
            meta_keywords,
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_relations') . ',
            categories,
            tags,
            related_portfolios,
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_images') . ',
            images,
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_files') . ',
            files,
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_markdown') . ',
            markdown,
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_language') . ',
            sys_language_uid;;;;1-1-1,
            l10n_parent,
            l10n_diffsource,
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_access') . ',
            hidden;;1,
            starttime,
            endtime
        '],
    ],
    'palettes' => [
        'sub_title_highlight' => ['showitem' => 'sub_title,highlight'],
        'geo_coordinates' => ['showitem' => 'latitude,longitude'],
    ],
    'columns' => [
        'information' => \CodingMs\AdditionalTca\Tca\Configuration::full('information', $table, $extKey),
        'sys_language_uid' => \CodingMs\Portfolios\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\Portfolios\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\Portfolios\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\Portfolios\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\Portfolios\Tca\Configuration::full('hidden'),
        'starttime' => \CodingMs\Portfolios\Tca\Configuration::full('starttime'),
        'endtime' => \CodingMs\Portfolios\Tca\Configuration::full('endtime'),
        'crdate' => [
            'exclude' => 0,
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('dateTime', false, true, '', ['dbType' => 'timestamp']),
        ],
        'tstamp' => [
            'exclude' => 0,
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('dateTime', false, true, '', ['dbType' => 'timestamp']),
        ],
        'title' => [
            'exclude' => 0,
            'label' => $lll . '.title',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('string', true),
        ],
        'sub_title' => [
            'exclude' => 0,
            'label' => $lll . '.sub_title',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('string'),
        ],
        'slug' => [
            'exclude' => 0,
            'label' => $lll . '.slug',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('slug', false, false, '', ['field' => 'title']),
        ],
        'highlight' => [
            'exclude' => 0,
            'label' => $lll . '.highlight',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('checkbox'),
        ],
        'teaser' => [
            'exclude' => 0,
            'label' => $lll . '.teaser',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled]',
        ],
        'description' => [
            'exclude' => 0,
            'label' => $lll . '.description',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled]',
        ],
        'images' => [
            'exclude' => 0,
            'label' => $lll . '.images',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('images')
        ],
        'files' => [
            'exclude' => 0,
            'label' => $lll . '.files',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('files')
        ],
        'working_hours' => [
            'exclude' => 0,
            'label' => $lll . '.working_hours',
            'description' => $lll . '.working_hours_description',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('int'),
        ],
        'finishing_date' => [
            'exclude' => 0,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => $lll . '.finishing_date',
            'description' => $lll . '.finishing_date_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('date', false, false, '', ['dbType' => 'timestamp']),
        ],
        'www' => [
            'exclude' => 0,
            'label' => $lll . '.www',
            'description' => $lll . '.www_description',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('string'),
        ],
        'latitude' => [
            'exclude' => 0,
            'label' => $lll . '.latitude',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('coordinate'),
        ],
        'longitude' => [
            'exclude' => 0,
            'label' => $lll . '.longitude',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('coordinate'),
        ],
        'attribute1' => [
            'exclude' => 0,
            'label' => $lll . '.attribute1',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('string'),
        ],
        'attribute2' => [
            'exclude' => 0,
            'label' => $lll . '.attribute2',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('string'),
        ],
        'attribute3' => [
            'exclude' => 0,
            'label' => $lll . '.attribute3',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('string'),
        ],
        'attribute4' => [
            'exclude' => 0,
            'label' => $lll . '.attribute4',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('string'),
        ],
        'attribute5' => [
            'exclude' => 0,
            'label' => $lll . '.attribute5',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('string'),
        ],
        'attribute6' => [
            'exclude' => 0,
            'label' => $lll . '.attribute6',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('string'),
        ],
        'attribute7' => [
            'exclude' => 0,
            'label' => $lll . '.attribute7',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('string'),
        ],
        'attribute8' => [
            'exclude' => 0,
            'label' => $lll . '.attribute8',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('string'),
        ],
        'html_title' => [
            'exclude' => 0,
            'label' => $lll . '.html_title',
            'description' => $lll . '.html_title_description',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('string'),
        ],
        'meta_abstract' => [
            'exclude' => 0,
            'label' => $lll . '.meta_abstract',
            'description' => $lll . '.meta_abstract_description',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('string'),
        ],
        'meta_description' => [
            'exclude' => 0,
            'label' => $lll . '.meta_description',
            'description' => $lll . '.meta_description_description',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('string'),
        ],
        'meta_keywords' => [
            'exclude' => 0,
            'label' => $lll . '.meta_keywords',
            'description' => $lll . '.meta_keywords_description',
            'config' => \CodingMs\Portfolios\Tca\Configuration::get('string'),
        ],
        'categories' => [
            'exclude' => 0,
            'label' => $lll . '.categories',
            'description' => $lll . '.categories_description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_portfolios_domain_model_portfoliocategory',
                'foreign_table_where' => 'AND tx_portfolios_domain_model_portfoliocategory.pid=###CURRENT_PID### AND tx_portfolios_domain_model_portfoliocategory.sys_language_uid < 1 ORDER BY tx_portfolios_domain_model_portfoliocategory.title ASC',
                'MM' => 'tx_portfolios_portfolio_portfoliocategory_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'wizards' => [
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                    'edit' => [
                        'type' => 'popup',
                        'title' => 'Edit',
                        'module' => [
                            'name' => 'wizard_edit'
                        ],
                        'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_edit.gif',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                    ],
                    'add' => [
                        'type' => 'script',
                        'title' => 'Create new',
                        'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_add.gif',
                        'params' => [
                            'table' => 'tx_portfolios_domain_model_portfoliocategory',
                            'pid' => '###CURRENT_PID###',
                            'setValue' => 'prepend'
                        ],
                        'module' => [
                            'name' => 'wizard_add'
                        ],
                    ],
                ],
            ],
        ],
        'tags' => [
            'exclude' => 0,
            'label' => $lll . '.tags',
            'description' => $lll . '.tags_description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_portfolios_domain_model_portfoliotag',
                'foreign_table_where' => 'AND tx_portfolios_domain_model_portfoliotag.pid=###CURRENT_PID### AND tx_portfolios_domain_model_portfoliotag.sys_language_uid < 1 ORDER BY tx_portfolios_domain_model_portfoliotag.title ASC',
                'MM' => 'tx_portfolios_portfolio_portfoliotag_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'wizards' => [
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                    'edit' => [
                        'type' => 'popup',
                        'title' => 'Edit',
                        'module' => [
                            'name' => 'wizard_edit'
                        ],
                        'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_edit.gif',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                    ],
                    'add' => [
                        'type' => 'script',
                        'title' => 'Create new',
                        'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_add.gif',
                        'params' => [
                            'table' => 'tx_portfolios_domain_model_portfoliotag',
                            'pid' => '###CURRENT_PID###',
                            'setValue' => 'prepend'
                        ],
                        'module' => [
                            'name' => 'wizard_add'
                        ],
                    ],
                ],
            ],
        ],
        'related_portfolios' => [
            'exclude' => 0,
            'label' => $lll . '.related_portfolios',
            'description' => $lll . '.related_portfolios_description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_portfolios_domain_model_portfolio',
                'foreign_table_where' => 'AND tx_portfolios_domain_model_portfolio.pid=###CURRENT_PID### AND tx_portfolios_domain_model_portfolio.sys_language_uid < 1 AND tx_portfolios_domain_model_portfolio.uid != ###THIS_UID### ORDER BY title',
                'MM' => 'tx_portfolios_portfolio_portfolioportfolio_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'wizards' => [
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                    'edit' => [
                        'type' => 'popup',
                        'title' => 'Edit',
                        'module' => [
                            'name' => 'wizard_edit'
                        ],
                        'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_edit.gif',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                    ],
                    'add' => [
                        'type' => 'script',
                        'title' => 'Create new',
                        'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_add.gif',
                        'params' => [
                            'table' => 'tx_portfolios_domain_model_portfolio',
                            'pid' => '###CURRENT_PID###',
                            'setValue' => 'prepend'
                        ],
                        'module' => [
                            'name' => 'wizard_add'
                        ],
                    ],
                ],
            ],
        ],
        'markdown' => [
            'exclude' => 0,
            'label' => $lll . '.markdown',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('markdown'),
            'defaultExtras' => 'fixed-font:enable-tab',
        ],
    ],
];

# Re-add the field for TYPO3 11 and return the $return.
if((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
}
return $return;
