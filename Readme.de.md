# Portfolios Erweiterung für TYPO3

Diese Extension stellt kategorisierte Portfolios/Referenzen für eine Website bereit.


**Features:**

*   Einbinden von Portfolios/Referenzen/Arbeitsbeispielen
*   Kategorien, Tags und Tag-Kategorien verfügbar
*   Optimiert für besseres Suchmaschinen-Ranking
*   Anzeigen von ähnlichen Portfolios/Referenzen
*   Flexible Listen und Detailansicht
*   Teaser-Ansichten möglich
*   Beliebig viele Bilder und Downloads je Portfolio/Referenz möglich
*   Vor- und Zurück-Link in Einzelansicht, um schnell zum nächste Portfolio/Referenz zu gelangen
*   Umkreissuche zum finden von Portfolios/Referenzen in der Nähe

**Pro-Features:**

*   Listen-Ansicht mit komplexen Multi-Filter via Isotope-Funktion
*   PDF-Generator zur dynamischen Erstellung von PDFs
*   Backend-Module zur einfacheren Verwaltung der Datensätze
*   Merkzettel für Portfolios/Referenzen
*   Merkzettel-PDF für dynamische Erstellung eines PDFs mit allen gemerkten Portfolios/Referenzen
*   Deckblatt und Inhaltsverzeichnis im Merkzettel-PDF möglich

Wenn ein zusätzliches oder individuelles Feature benötigt wird - kontaktiere uns gern!


**Links:**

*   [Portfolios Dokumentation](https://www.coding.ms/documentation/typo3-portfolios "Portfolios Dokumentation")
*   [Portfolios Bug-Tracker](https://gitlab.com/codingms/typo3-public/portfolios/-/issues "Portfolios Bug-Tracker")
*   [Portfolios Repository](https://gitlab.com/codingms/typo3-public/portfolios "Portfolios Repository")
*   [TYPO3 Portfolios Produktdetails](https://www.coding.ms/de/typo3-extensions/typo3-portfolios/ "TYPO3 Portfolios Produktdetails")
*   [TYPO3 Portfolios Dokumentation](https://www.coding.ms/de/dokumentation/typo3-portfolios "TYPO3 Portfolios Dokumentation")
*   [Demo: Drewes+Speth](https://www.drewes-speth.de/tragwerke "Demo: Drewes+Speth")
*   [Demo: coding.ms](https://www.coding.ms/portfolio "Demo: coding.ms")
*   [Demo: Gebo-Therm](https://www.gebotherm.com/unternehmen/referenzen/ "Demo: Gebo-Therm")
